# Support Pages

Where the markdown for our support pages lives. This projects acts as a
submodule on a Support Readiness v2 sync repo.

## Where should my file go?

It depends on the type of file and what Zendesk instances it will populate to:

| Type of file | Instances   | Folder to use                        |
|--------------|-------------|--------------------------------------|
| KB Article   | Both        | `knowledge-base/all_instances/`      |
|              | Global only | `knowledge-base/global_only/`        |
|              | US Gov only | `knowledge-base/us_government_only/` |
| Support Page | Both        | `support-pages/all_instances/`       |
|              | Global only | `support-pages/global_only/`         |
|              | US Gov only | `support-pages/us_government_only/`  |

## Knowledge base templates

You can find the knowledge base article templates in the 
[`kb-documentation/templates`](kb-documentation/templates) directory.

## What translates the markdown into HTML?

A combination of the
[GitLab Markdown API](https://docs.gitlab.com/ee/api/markdown.html) and some
custom code in the Support Readiness v2 sync repo convert the markdown into
HTML formatting needed for Zendesk Articles.

## Linking to internal pages

When you need to link to an internal page (i.e. a page managed via this repo),
a special format is needed to represent the URL:

```
[Text to display]({{LINK: filename#anchor}})
```

So to link to the "Reset your Password" section of the "Support Portal" page,
it is:

```
[Reset your password]({{LINK: portal.md#reset-your-password}})
```

The converter will translate that into the actual link needed on the HTML side.

And if you just wanted to do it without an anchor:

```
[Support Portal Page]({{LINK: portal.md}})
```

For any external links (i.e. something not managed via this repo), the standard
markdown link format will work.

## Custom Classes

This is a future iteration, so more info is TBD.

## GitLab Duo usage case

Duo Chat has context awareness and can read repository content from the page
that you're using Duo Chat from. By using Duo Chat on the same page as the KB
article template. As such, users of this project can navigate to the article
templates located within the
[`kb-documentation/templates`](kb-documentation/templates) directory and then
utilize GitLab Duo to help generate a knowledge base article for a prompt given
to it (specifically asking for it to use the template of the page you are on).
