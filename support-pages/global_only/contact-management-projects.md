# Contact Management Projects

**NOTE** If you are a member of a US Government pre-approved organizations, please see [For US Government pre-approved organizations]({{LINK: managing-support-contacts.md#for-us-government-pre-approved-organizations}}), as this section will not apply to you.

Contact management projects (abbreviated as CMPs) are projects on a GitLab Support Readiness controlled GitLab.com namespace that allows your organization to manage their support contacts more quickly than through tickets.

## Requesting a CMP

At your request, our Support Readiness team creates a collaboration project that is only accessible by the GitLab Support Readiness team and individuals you specify as allowed to manage the contacts.yaml file (up to 5 persons).

To utilize a contact management project, your organization must meet all the following criteria:

- Your organization must have an active subscription with GitLab (it cannot be expired or within the grace period)
- Your organization must have a valid subscription with GitLab
  - Valid subscriptions would be:
    - [Premium subscriptions](https://about.gitlab.com/pricing/premium/)
    - [Ultimate subscriptions](https://about.gitlab.com/pricing/ultimate/)
    - [A GitLab Dedicated subscription](https://about.gitlab.com/dedicated/)
- Your organization's valid subscription must be for _at least_ 50 seats
- The person requesting this must be one of the following:
  - The sold-to (purchasing party) of the subscription
  - An owner on the gitlab.com namespace

If you would like a contact management project setup for your organization (and you meet the above criteria), please contact the Support Readiness team by creating a new ticket via the [Support portal related matters form](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001801419).

## How it works

Commits made to the default branch (master) trigger a [GitLab webhook](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html) which in term triggers a CI/CD pipeline.

The CI/CD pipeline runs a job that fetches the project's contacts.yaml file and uses it to compare the entries against your organization's current support contacts:

- For any users missing, they get added to the list of support contacts
- For any users present in the contact management project (but not the contacts.yaml file), they are removed from the list of support contacts

After performing the sync operations, the CI/CD job will then update the [project badge](https://docs.gitlab.com/ee/user/project/badges.html) for your contact management project to reflect the sync's state.

## Using a CMP

### Adding a contact

To add a contact, add a new line at the bottom of the contact.yaml file in
the following format:

```
- name: 'NAME_OF_CONTACT'
  email: 'EMAIL_OF_CONTACT'
```

Replacing `NAME_OF_CONTACT` with the contact's name and `EMAIL_OF_CONTACT` with the email of the contact.

Commit the changes and the sync will take care of the rest!

### Removing a contact

To remove a contact, just delete the name and email rows from the file of the contact.

Commit the changes and the sync will take care of the rest!

### Maximum number of contacts

The maximum number of contacts this will manage is 50. Any entries after the first 50 are ignored by the sync mechanisms.

### Changing the developers who manage the contacts.yaml file

As this requires a heightened permission set, this can only be done by the Support Readiness team at this time. Please feel free to reach out to the Support Readiness team by creating a new ticket via the [Support portal related matters form](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001801419) if you would like to change the current list of developers for your contact management project.

### Checking the sync status

You can tell if the sync is in a good state or not by looking at the [project badge](https://docs.gitlab.com/ee/user/project/badges.html) for your contact management project. It will detail if the sync has been successful or has encountered issues. If you do see it has encountered issues, please feel free to reach out to the Support Readiness team by creating a new ticket via the [Support portal related matters form](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001801419).

## Requesting CODEOWNER usage

By default, the project we setup does not have CODEOWNER usage enabled. This is prevent any issues in your managing of the contacts.yaml file. Should your organization desire to have this functionality enabled, please reach out to the Support Readiness team by creating a new ticket via the [Support portal related matters form](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001801419) letting us know.

## Disabling a CMP

If after using a contact management project, your organization determines this is not the way you want to manage your support contacts, you can request it be disabled by reaching out to the Support Readiness team by creating a new ticket via the [Support portal related matters form](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001801419) letting us know.

Please note that once an organization has had a contact management project setup and later disabled, the option to use a contact management project in the future is no longer available.

## Questions?

If you have any questions regarding contact management projects, please reach out to the Support Readiness team by creating a new ticket via the [Support portal related matters form](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001801419) letting us know!
