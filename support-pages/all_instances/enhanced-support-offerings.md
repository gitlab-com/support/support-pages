# Support - Enhanced Support Offerings

Enhanced Support Offerings are paid for offerings in addition to the Support purchased with a subscription. 

## Assigned Support Engineer (ASE)

GitLab Premium and Ultimate subscriptions give you access to our Priority Support
Service.GitLab also offers Enhanced Support offerings including an add-on option of
an Assigned Support Engineer (ASE) who can provide a more personalized support
experience. The ASE is a senior level support engineer who can partner with you to
resolve support tickets more efficiently. 

Full description of offering is available as a [pdf download](https://drive.google.com/file/d/1I-GDQV9wZkTvTTMqIPw1mSLaeVUru4zU/view?usp=drive_link)
