# Support - Statement of Support

The GitLab Support Team is here to help Self-Managed, GitLab.com, and GitLab Dedicated customers. We want to ensure that GitLab works as expected with your existing code and infrastructure and we are happy to debug to ensure that the GitLab product works as expected within limits. This page details what is outside our scope of support.

Our responsibility is to ensure that GitLab’s core functionality works as designed in your environment. In cases where GitLab is not working as expected when integrated with your code and infrastructure, please contact us so we can investigate the issue within the limits defined in this document.

Troubleshooting GitLab involves ensuring that your code and infrastructure interact effectively with GitLab. Through the course of troubleshooting we may offer suggestions, resources, or issues from third parties. These resources are offered at the discretion of the [Support Engineer](https://handbook.gitlab.com/job-families/engineering/support-engineer/) and are provided for informational purposes as directly troubleshooting these components often falls [outside our scope of support](https://about.gitlab.com/support/statement-of-support/#out-of-scope).

## GitLab Self-Managed

### Starter (Legacy), Premium, and Ultimate Customers

We will help troubleshoot all components bundled with GitLab Omnibus when used as a packaged part of a GitLab installation. Any assistance with modifications to GitLab, including new functionality, bug-fixes, issues with [experiment features]({{LINK: statement-of-support.md#experiment-features}}) or other code changes should go through the [GitLab project's issue tracker](https://gitlab.com/gitlab-org/gitlab/issues). Moreover, support is not offered for local modifications to GitLab source code.

We understand that GitLab is often used in complex environments in combination with a variety of tools. We'll use **commercially-reasonable efforts** to debug components that work alongside GitLab.

If you obtained an Ultimate license as part of [GitLab's Open Source or Education program](https://handbook.gitlab.com/handbook/marketing/developer-relations/community-programs/open-source-program/), support is **not** included. Please see the [GitLab OSS License/Subscription Details](https://handbook.gitlab.com/handbook/marketing/developer-relations/community-programs/open-source-program/) for additional details.

### Version Support

Unless otherwise specified in your support contract, we support the current major version and previous two major versions only. For example, as [`17.x` is the current major version](https://about.gitlab.com/releases/), GitLab installations running versions in the `17.x`, `16.x` and `15.x` series are eligible for support.

GitLab only backports fixes, not features and that too on a limited number of prior releases. For more details please visit the [maintenance policy](https://docs.gitlab.com/ee/policy/maintenance.html).

If you contact support about issues you're experiencing while on an unsupported version, we'll link to this section of this page and invite you to upgrade.

### Installation & New Features

For assistance with first-time installations and the configuration of new features, we highly recommend using our comprehensive [documentation](https://docs.gitlab.com/):

- First-time installation [step by step guide](https://about.gitlab.com/install/)
- [Updating your GitLab instance](https://docs.gitlab.com/ee/update/)
- [High availability](https://docs.gitlab.com/ee/administration/reference_architectures/index.html)
- [Kubernetes clusters](https://docs.gitlab.com/ee/user/project/clusters/)

Please make sure to use the matching version of our documentation for the version of GitLab you plan to install. Our [Docs archives](https://docs.gitlab.com/archives/) site explains how to access older versions.

Alternatively, you can [reach out to our Professional Services team](https://about.gitlab.com/services/) who provide training and assistance with the design and implementation of new features and installations.

If you're facing challenges after attempting the installation of a new deployment or implementing new features, a support ticket can be opened using our [support portal]({{LINK: index.md#contact-support}}).

### Free and Community Edition Users

If you are seeking help with your GitLab Free or Community Edition installation, note that the GitLab Support Team is unable to directly assist with issues with specific installations of these versions. Please use the following resources instead:

- [GitLab Documentation](https://docs.gitlab.com/): Extensive documentation regarding the possible configurations of GitLab.
- [GitLab Community Forum](https://forum.gitlab.com/): This is the best place to have a discussion about your Community Edition configuration and options.
- [Stack Overflow](http://stackoverflow.com/questions/tagged/gitlab): Please search for similar issues before posting your own, as there's a good chance somebody else had the same issue as you and has already found a solution.

## GitLab.com

### Premium and Ultimate Customers

GitLab.com has a full team of Site Reliability Engineers and Production Engineers monitoring its status 24/7. This means that often, by the time you notice something is amiss, there's someone already looking into it.

We recommend that all GitLab.com customers follow [@gitlabstatus](https://twitter.com/gitlabstatus) on Twitter and use our [status page](https://status.gitlab.com/) to keep informed of any incidents.

If you obtained an Ultimate license as part of [GitLab's Open Source or Education program](https://handbook.gitlab.com/handbook/marketing/developer-relations/community-programs/open-source-program/), support is **not** included. Please see the [GitLab OSS License/Subscription Details](https://handbook.gitlab.com/handbook/marketing/developer-relations/community-programs/open-source-program/) for additional details.

### Free Users

Technical and general support for those using the Free version of GitLab.com is “Community First”. Like many other free SaaS products, users are first directed to find support through community sources such as the following:

- [GitLab Documentation](https://docs.gitlab.com/): Extensive documentation on anything and everything GitLab.
- [GitLab Community Forum](https://forum.gitlab.com/): Get help directly from the community. When able, GitLab employees also participate and assist in answering questions.
- [Stack Overflow](http://stackoverflow.com/questions/tagged/gitlab): Please search for similar issues before posting your own, as there's a good chance someone else had the same issue as you and has already found a solution.

However, GitLab Support will assist Free users with the following types of issues:

- Your account or repository is in an unusable state. For example, you're unable to log in to your account or access a repository without receiving an error that you cannot bypass. Please note though that [Account Recovery and 2FA Resets support]({{LINK: index.md#account-recovery-and-2fa-resets}}) is not available for Free users.
- Requests related to an email you received regarding a GitLab.com security or production incident.

To receive help for these types of issues please [contact support]({{LINK: index.md#contact-support}}), and be aware that if a Support Engineer determines that your request is more appropriate for community resources they will direct you to them. Please also note that there are no guaranteed response times associated with support tickets submitted by Free users.

### Buying a subscription mid-ticket

Once a ticket is filed, the customer's current plan is used for the ticket. If you purchase a subscription during the life of a ticket, file a new ticket in order to have the updated service reflected properly in the support portal and in the execution of support.

**Note**: Purchasing a plan to circumvent restricted support features (such as 2FA resets or name-squatting requests) is prohibited. We do not encourage you to do this as the service requested is still likely to be denied.

### GitLab.com Availability

You should follow [@gitlabstatus](https://twitter.com/gitlabstatus) on Twitter for status updates on GitLab.com, or check our [status page](https://status.gitlab.com/) to see if there is a known service outage and follow the linked issue for more detailed updates.

## GitLab Dedicated

This section describes our approach to supporting GitLab Dedicated customers.

### Access to GitLab Dedicated Tenant

The GitLab Support team does not have access to the GitLab application running on GitLab Dedicated tenants. In order to work effectively with Support, customers will need to share information from the GitLab application in support tickets.

## GitLab Duo Pro

If you encounter usability or performance issues while using GitLab Duo Pro features, we encourage you to submit a support ticket as we are committed to providing comprehensive support in line with the status of the specific features as covered in our [Statement of Support for Experiment and Beta Features](https://about.gitlab.com/support/statement-of-support/#experiment-beta-features). For a comprehensive list of GitLab Duo features and their current status, please see the [GitLab Duo documentation](https://docs.gitlab.com/ee/user/ai_features.html).


Please note that our Scope of Support does not cover the accuracy of, nor the responses generated by GitLab Duo Pro. If you would like to provide feedback on the accuracy of the data or responses generated, please refer to the "Feedback" section of the feature-specific documentation linked from the [GitLab Duo documentation](https://docs.gitlab.com/ee/user/ai_features.html)
in the [Statement of Support](https://about.gitlab.com/support/statement-of-support/)

## Out of Scope

The following sections outline what is within the scope of support and what is not for GitLab Self-Managed customers, GitLab.com customers, and both customers and Free users of either.

### GitLab Self-Managed Customers

| Out of Scope  | Example  |  What's in-scope then? |
|---|---|---|
| Debugging EFS problems  | *GitLab is slow in my HA setup. I'm using EFS.*  |  EFS and GlusterFS are not recommended for HA setups (see our [HA on AWS doc](https://docs.gitlab.com/ee/install/aws/index.html)). GitLab Support can help verify that your HA setup is working as intended, but will not be able to investigate EFS or GlusterFS backend storage issues. |
| Debugging git repository issues stored on NFS | *Commits vanished from our `main` branch. We're running 3 Gitaly servers, sharing data using NFS.*  | [NFS cannot be used for repository storage](https://docs.gitlab.com/ee/administration/nfs.html) |
| Troubleshooting non-GitLab components | *I'm trying to get GitLab to work with Apache, can you provide some pointers?* |  GitLab Support will only assist with the specific components and versions that ship with the official GitLab packages, and only when used as a part of a GitLab installation. |
| Local modifications to GitLab  | *We added a button to ring a bell in our office any time an MR was accepted, but now users can't log in.* |  GitLab Support would direct you to create a feature request or submit a merge request for code review to incorporate your changes into the GitLab core. |
| Old versions of GitLab  | *I'm running GitLab 7.0 and X is broken.* |  GitLab Support will invite you to upgrade your installation to a more current release. Only the current and two previous major versions are supported. |
| Instance migration configuration and troubleshooting  | *We migrated GitLab to a new instance and cannot SSH into the server.* |  GitLab Support will assist with issues that arise from the GitLab components. GitLab Support will not be able to assist with any issues stemming from the server or its configuration (see GitLab Instance Migration on the [Support page]({{LINK: general-policies.md#gitlab-instance-migration}})). |
| Debugging custom scripts and automations  | *We use custom scripts to automate changes to our GitLab deployment, and it is causing problems or downtime. Please create a custom script for me to leverage the GitLab API.*|  GitLab Support will assist in troubleshooting and resolving issues that occur in the course of interacting with an existing GitLab installation. GitLab Support will not be able to assist with debugging or fixing customer-written code used to deploy, upgrade or modify an in-place installation, or with creating custom scripts to use GitLab components. |
| Installation of GitLab using unofficial, community-contributed methods | *We ran into an error installing GitLab using the FreeBSD package. Please help!* | GitLab Support can only provide support for installation problems encountered when using an [official installation method](https://about.gitlab.com/install/).  |
| Upgrade Assistance for GitLab installed using unofficial, community-contributed methods  | *We installed GitLab using the Arch Linux community package and would like to request upgrade assistance* | GitLab Support can only provide Upgrade Assistance when GitLab is installed using an [official installation method](https://about.gitlab.com/install/).  |
| Step-by-step instructions for upgrading GitLab on self-hosted infrastructure  | *We installed one of the HA reference architectures and need to know how and in which order to update the specific nodes* | GitLab documentation describes the parts of a system that need to be upgraded, and describes certain dependencies. Because so many configurations are possible, we do not attempt to document all of them. We do provide support for the GitLab components of your setup, and can provide [Upgrade Assistance]({{LINK: scheduling-upgrade-assistance.md}}), including a review of your upgrade plan.  |
| Running raw SQL queries that modify the GitLab database  | *We used a mass update query in the GitLab database to change all of our usernames, and we are now experiencing issues as a result of that change.* | GitLab Support will direct you to roll back to a known working backup, and then assist with debugging and resolving the problem via a safer method.  |

### GitLab.com Customers

|  Out of Scope |  Example(s) | What's in-scope then?  |
|---|---|---|
| Troubleshooting non-GitLab components  | *How do I use git via CLI? How do I use a third party's IDE?* | GitLab Support will happily answer any questions and help troubleshoot any of the components of GitLab  |
| Consulting on language or environment-specific configuration  | *I want to set up a YAML linter CI task for my project. How do I do that?* | The Support Team will help you find the GitLab documentation for the related feature and can point out common pitfalls when using it.  |

## All Self-Managed & SaaS Users

### Training

GitLab Support is unable to provide training on the use of the underlying technologies that GitLab relies upon. GitLab is a product aimed at technical users, and we expect our users and customers to be versed in the basic usage of the technologies related to features that they're seeking support for.

For example, a customer looking for help with a Kubernetes integration should understand Kubernetes to the extent that they could retrieve log files or perform other basic tasks without in-depth instruction.

### Git and LFS

We're unable to assist in troubleshooting issues with specific Git commands and cannot provide training for using Git. If the latter is needed, [official Git documentation](https://git-scm.com/docs) is comprehensive. The same applies to [Git Large File Storage](https://git-lfs.github.com/). For both topics, GitLab-relevant aspects are collected in [our summary documentation](https://docs.gitlab.com/ee/topics/git/).

### CI/CD

GitLab Support cannot implement specific CI/CD solutions for you.
            For example:

- **In Scope**: I implemented a CI/CD configuration to run tests on all my branches, but it doesn't work as I expected it to based on the documentation.
- **Out of Scope**: I need a CI/CD configuration that runs tests on all my branches.

GitLab Support cannot assist with debugging specific commands or scripts included in your `.gitlab-ci.yml` file. We also cannot troubleshoot issues outside of the configuration or setup of private GitLab Runner host machines.

For example:

- **In Scope**: I'm including a custom script in my `.gitlab-ci.yml` file, but it appears that GitLab does not actually execute it.
- **Out of Scope**: I'm including a custom script in my `.gitlab-ci.yml` file and can't get the script to work on my new GitLab Runner which uses a different Linux version than my existing GitLab Runners.

### CI/CD Templates and Components

GitLab provides a range of [CI/CD templates](https://gitlab.com/gitlab-org/gitlab/tree/master/lib/gitlab/ci/templates) that you can include in your pipeline.
Some of these templates enable features like [Secret Detection](https://docs.gitlab.com/ee/user/application_security/secret_detection/) and [Static Application Security Testing](https://docs.gitlab.com/ee/user/application_security/sast/).
Other templates are [examples](https://docs.gitlab.com/ee/ci/examples/) designed to demonstrate functionality like [Load Performance Testing](https://docs.gitlab.com/ee/ci/testing/load_performance_testing.html) or [Accessibility testing](https://docs.gitlab.com/ee/ci/testing/accessibility_testing.html).
Going forward, some of this functionality may be delivered via [CI/CD Components](https://docs.gitlab.com/ee/ci/components/) as well.

GitLab Support will assist with documented uses of these templates and components. Assistance with direct modifications or undocumented uses is out of scope for GitLab Support. We welcome merge requests to improve the functionality of templates and components.

For assistance with CI/CD Components listed in the [CI/CD Catalog](https://docs.gitlab.com/ee/ci/components/#cicd-catalog), GitLab Support will refer you to the maintainer(s) of the corresponding [component project](https://docs.gitlab.com/ee/ci/components/index.html#component-project).

For example:

- **In Scope**:
  - I'm using one of the [CI/CD variables for SAST](https://docs.gitlab.com/ee/user/application_security/sast/#available-cicd-variables), or [customizing my Secret Detection ruleset](https://docs.gitlab.com/ee/user/application_security/secret_detection/pipeline/#customize-analyzer-rulesets), and it doesn't seem to be working as documented.
  - I want to [change the CI/CD stage](https://docs.gitlab.com/ee/user/application_security/#using-a-custom-scanning-stage) that my security scanning jobs run in, and I'm having trouble.
- **Out of Scope**:
  - I copied [`Jobs/SAST.gitlab-ci.yml`](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/SAST.gitlab-ci.yml) into my repository and I'm trying to modify it.
  - I run a custom `before_script` on a security scanning job, and now a package or file I need is no longer available.

### Third Party Applications & Integrations

GitLab Support cannot assist with the configuration or troubleshooting of third party applications, integrations, and services that may interact with GitLab. We can ensure that GitLab itself is sending properly formatted data to a third party in the bare-minimum configuration.

For example:

- *"I can't get Jenkins to run builds kicked off by GitLab. Please help me figure out what is going on with my Jenkins server."*

### Publicly available internal Tooling

We provide most of our internal tooling publicly. Examples are the [Congregate](https://gitlab.com/gitlab-org/professional-services-automation/tools/migration/congregate) tool that Professional Services uses for migrations and the [gitlab-triage](https://gitlab.com/gitlab-org/ruby/gems/gitlab-triage) gem that can automatically triage issues and merge requests. While you're free to use these tools to your benefit, GitLab Support cannot assist you with them. Please use the appropriate issue trackers or documented processes if you encounter problems.

### Basic reporting or data analysis

Support engineers are not data analysts, so any data analysis request
should be in pursuit of understanding and solving a problem.
GitLab Support can not assist with extracting very basic or differently purposed insights
from instance databases or repositories or log files.

For customer self-service, we collect useful examples of data extraction from
[repositories](https://docs.gitlab.com/ee/user/project/repository/reducing_the_repo_size_using_git.html),
[log files](https://docs.gitlab.com/ee/administration/logs/log_parsing.html),
and [share](https://gitlab.com/gitlab-com/support/toolbox/list-slowest-job-sections/)
our [various](https://gitlab.com/gitlab-com/support/toolbox/dotfiles#usage)
internal [tools](https://gitlab.com/gitlab-com/support/toolbox/fast-stats/#when-to-use-it).

For example:

- **In Scope**: I've analyzed our GitLab logs and found these errors. How should we resolve them?
- **In Scope**: I've started a [docs-comment issue](https://gitlab.com/gitlab-org/gitlab/-/issues/new?issue[description]=Link%20the%20doc%20and%20describe%20what%20is%20wrong%20with%20it.%0A%0A%3C!--%20Don%27t%20edit%20below%20this%20line%20--%3E%0A%0A%2Flabel%20~documentation%20~%22docs\-comments%22%20&issue[title]=Docs%20feedback:%20Write%20your%20title)/MR about querying these specific details: ...
  Can we collaborate to document a recommended example?
- **Out of Scope**: Our monitoring shows a ... spike. Please search through our logs to find what caused the spike.

### Configuration of Deployment Environments

GitLab Support cannot assist with infrastructure, in terms of the creation, configuration, or maintenance of on-premises, cloud, or infrastructure-as-code environments beyond the general guidance from what is provided in our documentation. This includes, but is not limited to, the configuration of firewalls, private networks, virtual machines, and secrets.

### Creation of SSL/TLS Certificates

GitLab Support cannot assist with the creation of SSL/TLS certificates, certificate signing requests, or the creation of certificate authorities.

## Experiment & Beta Features

### Experiment Features

[Experiment features](https://docs.gitlab.com/ee/policy/experiment-beta-support.html#experiment) are not yet completely tested for quality and stability, may contain bugs or errors, and prone to see breaking changes in the future. As such, support is not provided for these features and issues with them or other code changes should be opened in the [GitLab issue tracker](https://gitlab.com/gitlab-org/gitlab/issues).

### Beta Features

Your Support Contract will cover support for [Beta features](https://docs.gitlab.com/ee/policy/experiment-beta-support.html#beta). However, because they are not yet completely tested for quality and stability, may contain bugs or errors, and may be prone to see breaking changes in the future, troubleshooting will require more time, usually need assistance from Development, that support will be conducted on a **commercially-reasonable effort** basis.
