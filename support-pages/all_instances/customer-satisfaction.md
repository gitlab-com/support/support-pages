# Support - Customer Satisfaction

## Ticket satisfaction survey

24 hours after a ticket is Solved or automatically closed due to a lack of activity, a Customer Satisfaction survey will be sent out.

We track responses to these surveys through Zendesk with a target of 95% customer satisfaction.

Support Management regularly reviews responses, and may contact customers who leave negative reviews for more context.

### How is Support doing?

In the spirit of ["Is it any good?"](https://about.gitlab.com/why-gitlab) and GitLab's Value of <a href="/handbook/values/#transparency">Transparency</a> GitLab Support publishes its performance indicators publicly.

- [Customer Support Performance Indicators](https://handbook.gitlab.com/handbook/support/performance-indicators/)
- [Satisfaction with Support](https://handbook.gitlab.com/handbook/support/performance-indicators/#support-satisfaction-ssat)
- [SLA Achievement](https://handbook.gitlab.com/handbook/support/performance-indicators/#service-level-agreement-sla)
