# GitLab Support

There are many ways to contact Support, but the first step for most people should be to search our [documentation](https://docs.gitlab.com/).

## Contact Support

### Have questions about billing, purchasing, subscriptions or licenses?

We’re here to help! No matter what plan you have purchased, GitLab Support will respond within 8 hours on business days (24x5).

Open a Support Ticket on the [GitLab Support Portal]({{LINK: portal.md}}) and select [Subscription, License or Customers Portal Problems](https://about.gitlab.com/handbook/support/license-and-renewals/).

### Can’t find what you’re looking for?

If you can't find an answer to your question, or you are affected by an outage, then customers who are in a **paid** tier should start by consulting our [statement of support]({{LINK: statement-of-support.md}}) while being mindful of what is outside of the scope of support. Please understand that any support that might be offered beyond the scope defined there is done at the discretion of the agent or engineer and is provided as a courtesy.

### Managing your support contacts

All individuals who will open support tickets must be associated with an organization that holds a valid GitLab subscription. If you are not pre-listed this will result in a rejection message detailing you how to manage your organizations support contacts. Please see our dedicated page on [Managing Support Contacts]({{LINK: managing-support-contacts.md}}) for how to let GitLab Support know who is authorized to contact support.

## Find your support level

### GitLab Free Plan

- GitLab Free plan (GitLab.com or Self-managed) does not include support at any level. Instead, you can open a thread in the [GitLab Community Forum](https://forum.gitlab.com/).
- If you are facing issues relating to billing, purchasing, subscriptions, or licensing, please open a Support Ticket on the [GitLab Support Portal](https://support.gitlab.com/hc/en-us) and select [Subscription, License or Customers Portal Problems](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293).

### GitLab Premium/Ultimate Plan

- GitLab Premium plan comes with Priority Support for both GitLab.com and Self-Managed
- Tiered reply times based on [definitions of support impact]({{LINK: definitions.md#definitions-of-support-impact}}) for GitLab.com and Self-Managed
- Open a Support Ticket on the [GitLab Support Portal](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=334447) for GitLab.com and Self-Managed
- For emergency requests, see the note in the [How to Engage Emergency Support]({{LINK: index.md#how-to-engage-emergency-support}}) for Self-Managed.

### GitLab Trials

- Free GitLab Ultimate Self-managed and GitLab.com granted through trials do not include support at any level. If part of your evaluation of GitLab includes evaluating support expertise or SLA performance, please consider [contacting Sales](https://about.gitlab.com/sales/) to discuss options.
- If you are facing issues relating to billing, purchasing, subscriptions, or licensing, please open a Support Ticket on the [GitLab Support Portal](https://support.gitlab.com/hc/en-us) and select [Subscription, License or Customers Portal Problems](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293).

### GitLab Ultimate for Education

- Please see the [Support for Community Programs](https://docs.gitlab.com/ee/subscriptions/#support-for-community-programs) docs sections for a detailed description of where to find support. Please note that it is no longer an option to purchase support separately for GitLab for Education licenses. Instead, qualifying institutions have the option to purchase the [GitLab for Campuses subscription](https://about.gitlab.com/solutions/education/campus/).
- If you are facing issues relating to billing, purchasing, subscriptions, or licensing, please open a Support Ticket on the [GitLab Support Portal](https://support.gitlab.com/hc/en-us) and select [Subscription, License or Customers Portal Problems](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293).

### GitLab Ultimate for Open Source


- GitLab for Open Source programs do not come with technical support. Technical support for open source can, however, be purchased at a significant discount by [contacting Sales](https://about.gitlab.com/sales/).
- If you are facing issues relating to billing, purchasing, subscriptions, or licensing, please open a Support Ticket on the [GitLab Support Portal](https://support.gitlab.com/hc/en-us) and select [Subscription, License or Customers Portal Problems](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293).

### GitLab for Startups

- GitLab for Startups programs do not come with technical support. Technical support for open source can, however, be purchased at a significant discount by [contacting Sales](https://about.gitlab.com/sales/).
- If you are facing issues relating to billing, purchasing, subscriptions, or licensing, please open a Support Ticket on the [GitLab Support Portal](https://support.gitlab.com/hc/en-us) and select [Subscription, License or Customers Portal Problems](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293).

### GitLab Legacy Plans (Starter)

- GitLab Legacy plans come with [Standard Support]({{LINK: index.md#standard-support-legacy}}) for Starter Self-managed and will reply within 24 hours on business days (24x5).
- Open a Support Ticket on the [GitLab Support Portal](https://support.gitlab.com/).

### GitLab US Government Plans

- [US Federal Support]({{LINK: us-government-support.md#us-government-support}}) is available by opening a Support Ticket on the [GitLab Support Portal](https://gitlab-federal-support.zendesk.com/).
- For emergency requests, see the note in the [US Federal Support description]({{LINK: us-government-support.md#us-government-emergency-support}}).

### Alliance Partners

- Priority Support is available for [Alliance Partners](https://handbook.gitlab.com/handbook/support/partnerships/alliance/).
- Open a Support Ticket using the [GitLab Support Portal - Alliance Partner Form](https://support.gitlab.com/requests/new?ticket_form_id=360001172559).

## GitLab Support Service Levels

### Trials Support

Trial licenses do not include support at any level. If part of your evaluation of GitLab includes evaluating support expertise or SLA performance, please consider [contacting Sales](https://about.gitlab.com/sales/) to discuss options.

### Standard Support (Legacy)

Standard Support is included in Legacy GitLab self-managed Starter. It includes 'Next business day support' which means you can expect a reply to your ticket within 24 hours 24x5. (See [Support Staffing Hours]({{LINK: index.md#hours-of-operation}})).

### How to engage Emergency Support

To engage emergency support you should use the form for the support instance you are working out of:

- [Global Support](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001264259)
- [US Government Support](https://federal-support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001421112)

It is preferable to [include any relevant screenshots/logs in the initial ticket submission]({{LINK: general-policies.md#working-effectively-in-support-tickets}}). However if you already have an open ticket that has since evolved into an emergency, please include the relevant ticket number in the initial email.

- **Note:** For GitLab.com customers our infrastructure team is on-call 24/7 - please check [status.gitlab.com](https://status.gitlab.com/) before contacting Support.

Once an emergency has been resolved, GitLab Support will close the emergency ticket. If a follow up is required post emergency, GitLab Support will either continue the conversation via a new regular ticket created on the customer's behalf, or via an existing related ticket. Support Emergencies are defined as "GitLab instance in production is unavailable or completely unusable". The following is non-exhaustive list of situations that are generally considered as emergencies based on that definition, and a list of high priority situations that generally do not qualify as an emergencies according to that definition.

Emergency:

- GitLab instance is "down", unavailable, or completely unresponsive (for GitLab.com outages, an [Incident](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#incident-management) will be declared)
- GitLab always shows 4xx or 5xx errors on every page
- All users in an organization are unable to login to GitLab
- All users in an organization are unable to access their work on GitLab

Non-emergency:

- A single user is unable to login to GitLab.
- GitLab Runner or CI job execution is slower than usual.
- A CI pipeline is failing on one or more projects but not _all_ projects.
- One or more pages are not responding in browser, but most pages load successfully.
- An access token has stopped working or SSH key has expired.
- License/Subscription about to expire (there is a 14-day grace period following license expiration).
- GitLab application is usable but running slower than usual.
- Security incident affecting a publicly-accessible and unpatched self-managed GitLab server.
- A [Geo secondary](https://docs.gitlab.com/ee/administration/geo/#use-cases) is unhealthy or down.
- Elasticsearch integration is not working.

To help Support accurately gauge the severity and urgency of the problem, please provide as many details about how the issue is impacting or disrupting normal business operation when submitting the emergency ticket.

## Service Level Agreements

- GitLab offers 24x5 support (24x7 for Priority Support Emergency tickets) bound by the SLA times listed above.
- The SLA times listed are the time frames in which you can expect the first response.
- GitLab Support will make a best effort to resolve any issues to your satisfaction as quickly as possible. However, the SLA times are not to be considered as an expected time-to-resolution.

## Hours of Operation

### Definitions of GitLab Global Support Hours

- 24x5 - GitLab Support Engineers are actively responding to tickets Sunday 3pm Pacific Time through Friday 5pm Pacific Time.
- 24x7 - For Emergency Support there is an engineer on-call 24 hours a day, 7 days a week.


### Effect on Support Hours if a preferred region for support is chosen

When submitting a new ticket, you are asked to select a 'Preferred Region for Support'. Please select the region that most closely aligns with your working hours. This helps us assign Support Engineers located in your preferred region.  

For reference, the business hours for the various regions are as follows:

- Asia Pacific (APAC): 09:00 to 21:00 AEST (Brisbane), Monday to Friday
- Europe, Middle East, Africa (EMEA): 08:00 to 18:00 CET Amsterdam, Monday to Friday
- Americas (AMER): 05:00 to 17:00 PT (US & Canada), Monday to Friday

### Phone and video call support

GitLab does not offer support via inbound or on-demand calls.

GitLab Support Engineers communicate with you about your tickets primarily through updates in the tickets themselves. At times it may be useful and important to conduct a call, video call, or screensharing session with you to improve the progress of a ticket. The support engineer may suggest a call for that reason. You may also request a call if you feel one is needed. Either way, the decision to conduct a call always rests with the support engineer, who will determine:

- whether a call is necessary; and
- whether we have sufficient information for a successful call.

Once the decision has been made to schedule a call, the support engineer will:

- Send you a link (through the ticket) to our scheduling platform or, in the case of an emergency, a direct link to start the call.
- Update you through the ticket with: (a) an agenda and purpose for the call, (b) a list of any actions that must be taken to prepare for the call, and (c) the maximum time allowed for the call. Please expect that the call will end as soon as the stated purpose has been achieved or the time limit has been reached, whichever occurs first.

During a screensharing session Support Engineers will act as a trusted advisor: providing troubleshooting steps and inviting you to run commands to help gather data or help resolve technical issues. At no time will a GitLab Support Engineer ask to take control of your computer or to be granted direct access to your GitLab installation.

**Note:** Calls scheduled by GitLab Support are on the Zoom platform. If you cannot use Zoom, you can request a Cisco Webex link. If neither of these work for you, GitLab Support can join a call on the following platforms: Microsoft Teams, Google Hangouts, Zoom, Cisco Webex. Other video platforms are not supported.

**Please Note:** Attempts to reuse a previously-provided scheduling link to arrange an on-demand call will be considered an abuse of support, and will result in such calls being cancelled.

## Resources

Additional resources for getting help, reporting issues, requesting features, and so forth are listed on our get help page.

Or check our some of our other support pages:

- [Customer Satisfaction]({{LINK: customer-satisfaction.md}})
- [General Support Rules]({{LINK: general-policies.md}})
- [GitLab.com Specific Support Policies]({{LINK: gitlab-com-policies.md}})
- [Support Definitions]({{LINK: definitions.md}})
- [Support Portal]({{LINK: portal.md}})
- [US Government Support]({{LINK: us-government-support.md}})
- [Enhanced Support Offerings]({{LINK: enhanced-support-offerings.md}})
