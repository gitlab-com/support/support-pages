# Support - General Policies

## Differences Between Support Tickets and GitLab Issues

It's useful to know the difference between a support ticket opened through our [Support Portal](https://support.gitlab.com/) vs. [an issue on GitLab.com](https://gitlab.com/gitlab-org/gitlab/-/issues).

For customers with a license or subscription that includes support, always feel free to contact support with your issue in the first instance via [the Support Portal](https://support.gitlab.com/). This is the primary channel the support team uses to interact with customers and it is the only channel that is based on an SLA. Here, the GitLab support team will gather the necessary information and help debug the issue. In many cases, we can find a resolution without requiring input from the development team. However, sometimes debugging will uncover a bug in GitLab itself or that some new feature or enhancement is necessary for your use-case.

This is when we create an [issue on GitLab.com](https://gitlab.com/gitlab-org/gitlab/-/issues) - whenever input is needed from developers to investigate an issue further, to fix a bug, or to consider a feature request. In most cases, the support team can create these issues on your behalf and assign the correct labels. Sometimes we ask the customer to create these issues when they might have more knowledge of the issue or the ability to convey the requirements more clearly. Once the issue is created on GitLab.com it is up to the product managers and engineering managers to prioritize and drive the fix or feature to completion. The support team can only advocate on behalf of customers to reconsider a priority. Our involvement from this point on is more minimal.

## We don't keep tickets open (even if the underlying issue isn't resolved)

Once an issue is handed off to the development team through an issue in a GitLab tracker, the support team will close out the support ticket as Solved even if the underlying issue is not resolved. This ensures that issues remain the single channel for communication: customers, developers and support staff can communicate through only one medium.

## We handle each incident within a single support ticket

We handle a single issue or incident within a single support ticket. If you have multiple unrelated problems that need resolution, we recommend that you submit a separate ticket for each problem to ensure that each issue is tracked and addressed. There may be occasions when support engineers identify that problems or requests within a ticket are best handled in a new separate ticket. In situations such as these, the support engineer may create a new ticket on your behalf.

## Issue Creation

Building on the above section, when bugs, regressions, or any application behaviors/actions **not working as intended** are reported or discovered during support interactions, the GitLab Support Team will create issues in GitLab project repositories on behalf of our customers.

For feature requests, both involving the addition of new features as well as the change of features currently **working as intended**, support will request that the customer create the issue on their own in the appropriate project repos.

## Working Effectively in Support Tickets

As best you can, please help the support team by communicating the issue you're facing, or question you're asking, with as much detail as available to you. Whenever possible, include:

- [log files relevant](https://docs.gitlab.com/ee/administration/logs/#gathering-logs) to the situation.
- steps that have already been taken towards resolution.
- relevant environmental details, such as the architecture.

We expect for non-emergency tickets that GitLab administrators will take 20-30 minutes to formulate the support ticket with relevant information. A ticket without the above information will reduce the efficacy of support.

In subsequent replies, the support team may ask you follow-up questions. Please do your best to read through the entirety of the reply and answer any such questions. If there are any additional troubleshooting steps, or requests for additional information please do your best to provide it.

The more information the team is equipped with in each reply will result in faster resolution. For example, if a support engineer has to ask for logs, it will result in more cycles. If a ticket comes in with everything required, multiple engineers will be able to analyze the problem and will have what is necessary to further escalate to developers if so required.

## Automated Follow-up for Pending Cases

The support teams strive to provide quick and efficient responses to technical challenges. In order to help gather the necessary details and efficiently troubleshoot issues it may be necessary to request some additional information from case submitter. When this is done the ticket will go into a pending state to indicate support is waiting to hear back with the results of the troubleshooting or more information. After 7 days in `pending`, an automated follow-up will occur reminding the case's participants that the case is waiting for a response with additional details. If the issue is resolved you can [solve the case yourself]({{LINK: general-policies.md#can-users-solve-tickets-themselves}}), otherwise the case will solve itself out in another 7 days. If the issue is not resolved by the information the support engineer had provided, you can reply to the case and provide the current status of the troubleshooting, along with any information the engineer may have requested, and the case will continue once more.

## Please follow our Code of Conduct

If your ticket contains language or behavior that goes against the [GitLab Community Code of Conduct](https://about.gitlab.com/community/contribute/code-of-conduct/), you'll receive the following response and have your ticket closed:

> Hello,
>
> While we would usually be very happy to help you out with any issue, we cannot assist you on this ticket due to the language used not adhering to the [GitLab Community Code of Conduct](https://about.gitlab.com/community/contribute/code-of-conduct/). As noted in our [Statement of Support]({{LINK: general-policies.md#please-follow-our-code-of-conduct}}), we're closing this ticket.
>
> Please create a new ticket that complies with our guidelines and one of our support engineers will be happy to assist you.
>
> Thank you,

Repeat violations may result in termination of your support contract and/or business relationship with GitLab.

## Please don't send encrypted messages

Some organizations use 3rd party services to encrypt or automatically expire messages. As a matter of security, please do not include any sensitive information in the body or attachments in tickets. All interactions with GitLab Support should be in plain-text.

GitLab Support Engineers will not click on URLs contained within tickets or interact with these type of 3rd party services to provide support. If you do send such a reply, an engineer will politely ask you to submit your support ticket in plain text, and link you to this section of our Statement of Support.

If there's a pressing reason for which you need to send along an encrypted file or communication, please discuss it with the Engineer in the ticket.

## Please don't send files in formats that can contain executable code

Many common file formats, such as Microsoft Office files and PDF, can contain executable code which can be run automatically when the file is opened. As a result, these types of files are often used as a computer attack vector.

GitLab Support Engineers will not download files or interact with these type of files. If you send a file in a format of concern, an engineer will ask you to submit your attachments in an accepted format, and link you to this section of the Support page.
Examples of acceptable formats include:

- **Preferred**: Plain Text files (.txt, .rtf, .csv, .json, .yaml)
- Images (.jpg, .gif, .png) as an alternative to PDF, which can be converted using [pdftoppm](https://linux.die.net/man/1/pdftoppm). Ex: `pdftoppm -png path/to/file.pdf output_file_name`

## Prefer copy-pasting text over screenshots

Screenshots are helpful if UI or front-end problems need to be explained.
In most other cases, the most relevant content is usually command output, log messages, or other text.
While [OCRing is possible](https://support.apple.com/en-gb/guide/preview/prvw625a5b2c/mac),
it can introduce subtle mistakes.
The most efficient option for both customers and Support Engineers is to copy-and-paste text,
rather than screenshot it.

## Please don't share login credentials

Do not share login credentials for your GitLab instance to the support team. If the team needs more information about the problem, we will offer to schedule a call with you.

## Do not contact Support Engineers directly outside of issues or support cases

GitLab Support engages with users via the GitLab Support Portals and GitLab.com issues or merge requests only. Using these scoped methods of communication allows for efficiency and collaboration, and ensures both parties' safety while addressing problems that may arise. No attempts should be made to receive support via direct individual email, social media, or other communication methods used by GitLab team members. Use of these unofficial methods to obtain support may be considered abuse of the platform. Repeat violations may result in termination of your support contract and/or business relationship with GitLab.

If you contact a Support Engineer directly, they'll reply with a short explanation and a link to this paragraph.

## Sanitizing data attached to Support Tickets

If relevant to the problem and helpful in troubleshooting, a GitLab Support Engineer will request information regarding configuration files or logs.

We encourage customers to sanitize all secrets and private information before sharing them in a GitLab Support ticket.

Sensitive items that should never be shared include:

- credentials
- passwords
- tokens
- keys
- secrets

There's more specific information on the dedicated [handling sensitive information with GitLab Support]({{LINK: sensitive-information.md}}) page.

## Support for GitLab on restricted or offline networks

GitLab Support may request logs in Support tickets or ask you to screenshare in customer calls if it would be the most efficient and effective way to troubleshoot and solve the problem.

Under certain circumstances, sharing logs or screen sharing may be difficult or impossible due to our customers' internal network security policies.

GitLab Support will never ask our customers to violate their internal security policies, but Support Engineers do not know the details of our customers' internal network security policies.

In situations where internal or network security policies would prevent you from sharing logs or screen sharing with GitLab Support, please communicate this as early as possible in the ticket so we can adjust the workflows and methods we use to troubleshoot.

Customer policies preventing the sharing of log or configuration information with Support may lengthen the time to resolution for tickets.

Customer policies preventing screen sharing during GitLab Support customer calls may impact a Support Engineer's ability to resolve issues during a scheduled call.

## Including colleagues in a support ticket

Our Support Portal will automatically drop any CCed email addresses for tickets that come in via email. If you would like to include colleagues in a support interaction, you must be logged into the Support Portal. From there, the option to add your colleagues as CCs will be available. Requests to add colleagues through the support ticket will not be actioned on.

To view tickets you have been CCed on, navigate to **Profile Icon > My activities > Requests I'm CC'd on**.

For more detailed instructions, take a look at our [Support Portal Documentation]({{LINK: portal.md#adding-additional-participants-ccs-to-your-ticket}}).

## Solving Tickets

There are two scenarios where GitLab will solve an existing ticket:

* A customer explains that they are satisfied that their concern has been addressed properly in the ticket
* The ticket has been in a pending state for 14 consecutive days

After a ticket is solved, it can be re-opened for the next 7 days should a customer wish to do so by either replying to the notification emails for the ticket or logging into the support portal to reply to the ticket.

In the event the ticket has not been re-opened within 7 days, the ticket will be marked as closed. In this state, it can not be updated and a new ticket will need to be created (either manually or by making a follow-up ticket via the support portal). Should a customer reply to a closed ticket, an automation will notify them that the ticket could not be re-opened and they will need to use one of the aforementioned methods to generate a new ticket.

## Can Users Solve Tickets Themselves?

A user can solve a ticket themselves by logging into the Support Portal and navigating to the [My activities](https://support.gitlab.com/hc/en-us/requests) page. From there the user can select one of their tickets that is in an open or pending state, navigate to the bottom of the ticket notes section, check the `Please consider this request solved` box and press `Submit`. If the user would like to reopen the case they can simply respond with a new comment and the case will reopen or create a follow up case.

## GitLab Instance Migration

If a customer requests assistance in migrating their existing self-hosted GitLab to a new instance, you can direct them to our [Migrate to a New Server](https://docs.gitlab.com/ee/administration/backup_restore/migrate_to_new_server.html) documentation. Support will assist with any issues that arise from the GitLab migration. However, the setup and configuration of the new instance, outside of GitLab specific configuration, is considered out of scope and Support will not be able to assist with any resulting issues.

## Requesting Assignment to a Specific Support Engineer

Because a Support Engineer's availability to take on tickets varies, we typically don't grant requests to assign a ticket to a specific Support Engineer. We suggest customers [select a specific region when creating a ticket]({{LINK: index.md#effect-on-support-hours-if-a-preferred-region-for-support-is-chosen}}), and ask for the ticket to be handled by Support Engineers in that region.

Alternatively, our [Assigned Support Engineer (ASE) Support offering]({{LINK: enhanced-support-offerings.md#assigned-support-engineer-ase}}) provides a dedicated Support Engineer to customers looking to consolidate troubleshooting efforts.