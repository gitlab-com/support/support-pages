# Managing Support Contacts

The GitLab support team is here to help. By providing a list of named support contacts your team can quickly collaborate with GitLab Support on any issues you encounter. To keep sensitive ticket data within your span of control: if a ticket is submitted by someone not on your list, the ticket will be rejected and directed to this page.

## Initial setup

**NOTE** If you are a member of a US Government pre-approved organizations, please see [For US Government pre-approved organizations]({{LINK: managing-support-contacts.md#for-us-government-pre-approved-organizations}}) below, as this section will not apply to you.

While our back-end sync attempts to automatically add a support contact because it was a brand new organization within our support system and the support contact was the sold-to of the subscription, the remaining support contacts require manual intervention to get populated. As such, you will need to create a ticket so we can work with you in this endeavor. When creating the ticket, the form you want to use is `Support portal related matters`:

- Individuals, business and enterprise customers, use the [Global Support Portal new ticket link](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001801419)
- For US Government pre-approved organizations, use the [US Government Support Portal new ticket link](https://federal-support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001421052)

## Modifying support contacts after initial setup

**NOTE** If you are a member of a US Government pre-approved organizations, please see [For US Government pre-approved organizations]({{LINK: managing-support-contacts.md#for-us-government-pre-approved-organizations}}) below, as this section will not apply to you.

We currently offer two methods to manage your support contacts:

1. Via ticket, which is done by submitting a `Support portal related matters` ticket to our Support Readiness team
   - This has a limit of 30 maximum contacts under your organization.
2. Via a [contact management project](https://support.gitlab.com/hc/en-us/articles/14142703050396-Contact-Management-Projects)
   - This has a limit of 50 maximum contacts under your organization.

If utilizing the first option, you will need to submit a _new_ ticket using the `Support portal related matters` form. See [Creating a ticket]({{LINK: portal.md#f#creating-a-ticket}}) for more information.

For the best experience, try to be as specific as possible. Miscommunications can result in a very bad experience, so the more detail you provide in the ticket you file, the better and faster the results.

### Exceeding the maximum number of support contacts

Should a request to add more or setup a shared organization arise when at the limit (or when the request would put you over the limit), the Support Readiness team will discuss this with you to find a resolution.

### Using an email alias or distribution group as a support contact

Some organizations prefer to use a generic email address like an alias or distribution group for one of their registered support contacts. While this does work by itself, the end experience you will encounter may not be what you are aiming for. While it would be best to speak to the Support Readiness team via a new ticket to discuss options, our general advice for doing this would be:

1. Set a login password for this support user and share it within your team.
2. When you raise a ticket, always log in: this will allow you to add CCs to any tickets you raise.
3. CC any email addresses that may be involved in the resolution of the ticket: this will allow other individuals in the organization to reply to the ticket via email.

### Declaring who can request changes

For organizations that require additional security in regards to who can (and cannot) request changes to the support contacts, Support Readiness can help to setup your organization to ensure we adhere this request. If this is something your organization is interested in , please submit a _new_ ticket using the `Support portal related matters` form letting us know what you would like done. See [Creating a ticket]({{LINK: portal.md#f#creating-a-ticket}}) for more information.

## Proving support entitlement

**NOTE** If you are a member of a US Government pre-approved organizations, please see [For US Government pre-approved organizations]({{LINK: managing-support-contacts.md#for-us-government-pre-approved-organizations}}) below, as this section will not apply to you.

Depending on how you purchased your subscription, GitLab Support may not automatically detect your support entitlement on the creation of your first support ticket. If that's the case, your ticket might get a rejection message and direct you to this page. While we always aim to avoid this, in the interest of everyone's security, this is a path that can occur.

If you are speaking to our Support Readiness team, you might be asked to prove your entitlement. Depending on your type of subscription, the information we need to proceed can vary:

- For [Premium subscriptions](https://about.gitlab.com/pricing/premium/) and [Ultimate subscriptions](https://about.gitlab.com/pricing/ultimate/) on self-managed instances, we need **all of** the following information:
  - The request must be coming from a company provided email address (no generic email addresses such as Gmail, Yahoo, etc.)
  - Licensing information, which is one of the following:
    - The license ID displayed on the `/admin/subscription` page of your self-managed instance
    - The cloud activation code (obtainable by logging into customers.gitlab.com and viewing the subscription)
    - The raw license file uploaded to your self-managed instance (obtainable by logging into customers.gitlab.com, viewing the subscription, and clicking on the download link)
      - Alternatively, you can obtain the information by [exporting your license usage](https://docs.gitlab.com/ee/subscriptions/self_managed/index.html#export-your-license-usage) and reviewing the CSV it generates
  - The email of the person the subscription was sold to (the subscription owner)
- For [Premium subscriptions](https://about.gitlab.com/pricing/premium/) and [Ultimate subscriptions](https://about.gitlab.com/pricing/ultimate/) on gitlab.com, we need the following information:
  - The requester to be an `Owner` on the parent group with the paid subscription
- For [GitLab Dedicated subscriptions](https://about.gitlab.com/dedicated/), we need **all of** the following information:
  - The request must be coming from a company provided email address (no generic email addresses such as Gmail, Yahoo, etc.)
  - Licensing information, which is one of the following:
    - The license ID displayed on the `/admin/subscription` page of your GitLab Dedicated instance
    - The cloud activation code (obtainable by logging into customers.gitlab.com and viewing the subscription)
    - The raw license file uploaded to your GitLab Dedicated instance (obtainable by logging into customers.gitlab.com, viewing the subscription, and clicking on the download link)
      - Alternatively, you can obtain the information by [exporting your license usage](https://docs.gitlab.com/ee/subscriptions/self_managed/index.html#export-your-license-usage) and reviewing the CSV it generates
  - The URL of your GitLab Dedicated instance (usually provided initially when filing the ticket)
  - The email of the person the subscription was sold to (the subscription owner)

In cases where none of the above is able to be obtained, the only remaining method to prove your support entitlement would be for the subscription owner (the person it was sold to) to contact us via a ticket (and explain why none of the aforementioned method are able to be done).

If you are not able to prove your support entitlement when requested to do so, Support Readiness would not be able to proceed (and close out the ticket).

## Contact Management Project

**NOTE** If you are a member of a US Government pre-approved organizations, please see [For US Government pre-approved organizations]({{LINK: managing-support-contacts.md#for-us-government-pre-approved-organizations}}) below, as this section will not apply to you.

Contact management projects are a project on a GitLab Support Readiness controlled GitLab.com namespace that allows you and your team to add and remove support contacts more quickly than through tickets.

For more information on these, please see our [Contact Management page](contact-management-projects.html).

## For US Government pre-approved organizations

For security purposes, all customers of US Government support must be provisioned in the portal as support contacts via their account team. Please reach out to your Account manager, Customer Success contact(s), or our [sales team](https://about.gitlab.com/sales) for assistance in getting your support contacts setup for the support portal.

If you wish to have a [Shared Organization]({{LINK: managing-support-contacts.md#shared-organizations}}) setup or have [Special Handling Notes]({{LINK: managing-support-contacts.md#special-handling-notes}}) specified for your organization, please see the linked sections for more information on that.

## Shared Organizations

In some cases, certain organizations want all members of their organization to be able to see all of the support tickets that have been logged. This functionality is called a "Shared Organization". The options for a Shared Organization are as follows:

- All support contacts on the organization can view, but not comment, on all tickets within the organization
- All support contacts on the organization can view and comment on all tickets within the organization

Please keep in mind that using a shared organization entails a potential security risk. Namely, if all users can view and comment on all tickets, that means the degree of privacy and security from separating the tickets is gone. This won't mean those outside your organization can see your tickets, only those within your organization.

If you'd like to enable this for your organization in the support portal, please create a ticket using the `Support portal related matters` form:

- Individuals, business and enterprise customers, use the [Global Support Portal new ticket link](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001801419)
- For US Government pre-approved organizations, use the [US Government Support Portal new ticket link](https://federal-support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001421052)

## Special Handling Notes

We want your support experience to be the best it possible can be. If you want to discuss other potential options or handling instructions for your organization within the support portal, please let us know!

You can do so by creating a ticket using the `Support portal related matters` form:

- Individuals, business and enterprise customers, use the [Global Support Portal new ticket link](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001801419)
- For US Government pre-approved organizations, use the [US Government Support Portal new ticket link](https://federal-support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001421052)
