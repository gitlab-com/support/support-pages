# Support Portal

## GitLab teams that operate within the support portal

Several teams often operate within the support portal (and we often will refer to them when moving tickets or talking about where to submit a ticket). As a quick reference, the following tables should help you know what team you are speaking to within a ticket.

### For US Government pre-approved organizations

| Team | Forms |
|------|-------|
| Support | Technical Support Requests |
|   | Upgrade Planning Assistance Request |
|   | Emergency Support Request |
| Support Readiness | Support portal related matters |
| Licensing and Renewals team | License, Subscription, and Renewals Request |

### For anyone else

| Team | Forms |
|------|-------|
| Support | Support for GitLab.com |
|   | 2FA Reset |
|   | GitLab.com user accounts and login issues |
|   | Support for a self-managed GitLab instance |
|   | Support for GitLab Dedicated instances |
|   | Support for alliance partners |
|   | File an emergency request |
| Support Readiness | Support for alliance partners |
| Licensing and Renewals team | Subscription, License or Customers Portal Problems |
| Billing/Accounts Receivable | Billing inquiries/refunds |

## Account Management

### Creating a support portal account

**Note** If you have filed a ticket without signing into a support portal account previously or another associated contact had requested you be added to your organization, an account was already created for you. You will need to reset your password first to be able to login. See [reset your password]({{LINK: portal.md#reset-your-password}}) for more details.

The creation of a support portal account can happen one of three ways:

1. Our back-end sync automatically added you as a support contact because it was a brand new organization within our support system and you were the sold-to of the subscription. This is done with a "best attempt" basis (as many factors can prevent this action). You will need to reset your password first to be able to login if your account is created in this way. See [confirming your account]({{LINK: portal.md#reset-your-password}}) for more details.
1. An associated contact for your organization has already requested you be added to the organization as a support contact. In these cases, GitLab has already created your account as part of that process. You will need to reset your password first to be able to login if your account is created in this way. See [reset your password]({{LINK: portal.md#reset-your-password}}) for more details.
1. Manually by your action. This is done by:
   1. Navigating to the support portal:
      - Individuals, business and enterprise customers, use the [Global Support Portal link](https://support.gitlab.com/hc/en-us)
      - For US Government pre-approved organizations, use the [US Government Support Portal link](https://federal-support.gitlab.com/hc/en-us)
   1. Click the `Sign in` button at the top-right of the page:
      - Individuals, business and enterprise customers, use the [Global Support Portal signin link](https://gitlab.zendesk.com/auth/v2/login/signin)
      - For US Government pre-approved organizations, use the [US Government Support Portal signin link](https://gitlab-federal-support.zendesk.com/auth/v2/login/signin)
   1. Click the `Sign up` link in the bottom-left of the box:
      - Individuals, business and enterprise customers, use the [Global Support Portal registration link](https://gitlab.zendesk.com/auth/v2/login/registration)
      - For US Government pre-approved organizations, use the [US Government Support Portal registration link](https://gitlab-federal-support.zendesk.com/auth/v2/login/registration)
   1. Enter your full name and email address on the form
   1. Click the blue `Sign up` button
   1. Login to your email and locate the registration email you received. It will direct you to click a link to create a password.
      - Alternatively, you manually reset your password. See [Reset your password]({{LINK: portal.md#reset-your-password}}) for more details.

If you encounter any issues, please see our [Support Portal Troubleshooting]({{LINK: portal.md#support-portal-troubleshooting}}) section for further assistance.

### Logging into your support portal account

To login to your support portal account, you would:

1. Navigate to the support portal:
   - Individuals, business and enterprise customers, use the [Global Support Portal](https://support.gitlab.com/hc/en-us)
   - For US Government pre-approved organizations, use [US Government Support Portal link](https://federal-support.gitlab.com/hc/en-us)
1. Click the `Sign in` button at the top-right of the page:
   - Individuals, business and enterprise customers, use the [Global Support Portal signin link](https://gitlab.zendesk.com/auth/v2/login/signin)
   - For US Government pre-approved organizations, use the [US Government Support Portal signin link](https://gitlab-federal-support.zendesk.com/auth/v2/login/signin)
1. Utilize your preferred method of loggin in:
   - If you have a Twitter (X Corp), Facebook, Google, or Microsoft account using the _same_ email address as your support portal account, you can click one of the single sign-on (SSO) buttons on the left side of the box. Please note you will have approve the access on the SSO account for the support portal (Zendesk) to access read-only information on your profile.
   - Entering the email address and password set for your support portal account (and then clicking the blue Sign in button)

If you encounter any issues, please see our [Support Portal Troubleshooting]({{LINK: portal.md#support-portal-troubleshooting}}) section for further assistance.

### Reset your password

To reset your support portal's password (and also confirm the account), you would:

1. Navigate to the support portal:
   - Individuals, business and enterprise customers, use the [Global Support Portal](https://support.gitlab.com/hc/en-us)
   - For US Government pre-approved organizations, use the [US Government Support Portal link](https://federal-support.gitlab.com/hc/en-us)
1. Click the `Sign in` button at the top-right of the page:
   - Individuals, business and enterprise customers, use the [Global Support Portal signin link](https://gitlab.zendesk.com/auth/v2/login/signin)
   - For US Government pre-approved organizations, use the [US Government Support Portal signin link](https://gitlab-federal-support.zendesk.com/auth/v2/login/signin)
1. Click the `Forgot password?` link (below the Password input box):
   - Individuals, business and enterprise customers, use the [Global Support Portal password reset link](https://gitlab.zendesk.com/auth/v2/login/password_reset)
   - For US Government pre-approved organizations, use the [US Government Support Portal password reset link](https://gitlab-federal-support.zendesk.com/auth/v2/login/password_reset)
1. Enter your support portal account's email address
1. Click the blue Submit button

Doing so will send an email to your support portal account's email address. Within that email will be a link to set your new password. Clicking the link will bring you to a page to set your support portal account's new password.

If you encounter any issues, please see our [Support Portal Troubleshooting]({{LINK: portal.md#support-portal-troubleshooting}}) section for further assistance.

### Change your password

To change your support portal account's password, you would:

1. [Log into your support portal account]({{LINK: portal.md#logging-into-your-support-portal-account}})
1. Click your profile link in the top right of the page.
1. Click the `Change password` option on the drop-down menu.
1. Fill out the box that appears with the following:
   - Your support portal account's current password
   - The new password you want to use for your support portal account
   - The new password you want to use for your support portal account (entered again for security)
1. After doing so, click the black `Save` button at the bottom-right of the box

An alternative to doing this is to just reset your support portal account's password. See [Reset your password]({{LINK: portal.md#reset-your-password}}) for more details.

If you encounter any issues, please see our [Support Portal Troubleshooting]({{LINK: portal.md#support-portal-troubleshooting}}) section for further assistance.

### Change your name

To change the name used on your support portal account (and the name that GitLab Support will see in tickets), you would:

1. [Log into your support portal account]({{LINK: portal.md#logging-into-your-support-portal-account}})
1. Click your profile link in the top right of the page.
1. Click the `Profile` option on the drop-down menu.
1. Enter the new name you wish to use in the box that appears
1. Click the black `OK` button at the bottom-right of the box.

If you encounter any issues, please see our [Support Portal Troubleshooting]({{LINK: portal.md#support-portal-troubleshooting}}) section for further assistance.

### Change your profile picture

**NOTE** By default, our support portal will use the [gravatar image](https://gravatar.com/) associated with your support portal account's email address. If you do not have an existing gravatar image associated with your support portal account's email address, a default image is used.

To change the profile picture used for your support account, you would:

1. [Log into your support portal account]({{LINK: portal.md#logging-into-your-support-portal-account}})
1. Click your profile link in the top right of the page.
1. Click the `Profile` option on the drop-down menu.
1. Click the white `Change photo` button (to the right of the current profile picture).
1. Select the file to use for your profile picture from your computer
1. Click the black `OK` button at the bottom-right of the box.

If you encounter any issues, please see our [Support Portal Troubleshooting]({{LINK: portal.md#support-portal-troubleshooting}}) section for further assistance.

### Change your email

**NOTE** In cases where your organization is using a [Contact Management Project]({{LINK: managing-support-contacts.md#contact-management-project}}), changing your email might result in you being de-associated from your organization depending on your organization's setup. You should speak with your organization to ensure that changing your email is acceptable and will not cause issues.

To change the email used for your support portal account, you would:

1. [Log into your support portal account]({{LINK: portal.md#logging-into-your-support-portal-account}})
1. Click your profile link in the top right of the page.
1. Click the `Profile` option on the drop-down menu.
1. Enter the new email you wish to use in the box that appears
1. Click the black `OK` button at the bottom-right of the box.

If you encounter any issues, please see our [Support Portal Troubleshooting]({{LINK: portal.md#support-portal-troubleshooting}}) section for further assistance.

### Adding secondary email addresses

As the system does not currently allow this to be done via the support portal itself, please open a ticket directly to the Support Readiness team via the `Support portal related matters` form:

- Individuals, business and enterprise customers, use the [Global Support Portal new ticket link](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001801419)
- For US Government pre-approved organizations, use the [US Government Support Portal new ticket link](https://federal-support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001421052)

To expedite the process, please ensure you do the following when submitting the ticket:

- Be logged into your support portal account
- Specify the new email address you are wishing to have added to the support portal account

If you encounter any issues, please see our [Support Portal Troubleshooting]({{LINK: portal.md#support-portal-troubleshooting}}) section for further assistance.

### Logging out of your support portal account

To logout of your support account, you would:

1. Navigate to the support portal:
   - Individuals, business and enterprise customers, use the [Global Support Portal link](https://support.gitlab.com/hc/en-us)
   - For US Government pre-approved organizations, use the [US Government Support Portal link](https://federal-support.gitlab.com/hc/en-us)
1. Click your profile link in the top right of the page.
1. Click the Sign out link:
   - Individuals, business and enterprise customers, use the [Global Support Portal logout link](https://support.gitlab.com/access/logout)
   - For US Government pre-approved organizations, use the [US Government Support Portal logout link](https://federal-support.gitlab.com/access/logout)

If you encounter any issues, please see our [Support Portal Troubleshooting]({{LINK: portal.md#support-portal-troubleshooting}}) section for further assistance.

## Ticket Management

### Creating a ticket

To create a ticket via the support portal, you will want to navigate to the `Submit a ticket` page:

- Individuals, business and enterprise customers, use the [Global Support Portal new ticket link](https://support.gitlab.com/hc/en-us/requests/new)
- For US Government pre-approved organizations, use the [US Government Support Portal new ticket link](https://federal-support.gitlab.com/hc/en-us/requests/new)

The location of the button for this can be found in multiple locations for your convenience:

- At the top right on all support portal pages
- As one of the 6 block items on the main page (the 6th one on the bottom-right)

This page will then ask you to choose the reason you are reaching our to us. The answer on this question will determine the _form_ that is used for your ticket submission, which will be used to route your ticket to the correct team.

On each form, you will want to fill out as much information as is possible. The more detailed and thorough of a description given to the various GitLab teams, the quicker and more efficient the ticket will be resolved.

If you encounter any issues, please see our [Support Portal Troubleshooting]({{LINK: portal.md#support-portal-troubleshooting}}) section for further assistance.

#### Creating emergency tickets

When you are experiencing an emergency, it is often best to know the link to the direct form to get in contact with our Support team. To assist here, these would be the direct links to the emergency ticket form:

- Individuals, business and enterprise customers, use the [Global Support Portal new ticket link](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001264259)
- For US Government pre-approved organizations, use the [US Government Support Portal new ticket link](https://federal-support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001421112)

### Viewing your tickets

To view the tickets you have submitted, you would:

1. [Log into your account]({{LINK: portal.md#logging-into-your-support-portal-account}})
1. Click your profile link in the top right of the page
1. Click the Requests link:
  - Individuals, business and enterprise customers, use the [Global Support Portal requests link](https://support.gitlab.com/hc/en-us/requests)
  - For US Government pre-approved organizations, use the [US Government Support Portal requests link](https://federal-support.gitlab.com/hc/en-us/requests)

### Viewing tickets you are CC'd on

To view the tickets you have been CC'd on by your colleagues, you would:

1. [Log into your account]({{LINK: portal.md#logging-into-your-support-portal-account}})
1. Click your profile link in the top right of the page
1. Click the Requests link:
  - Individuals, business and enterprise customers, use the [Global Support Portal requests link](https://support.gitlab.com/hc/en-us/requests)
  - For US Government pre-approved organizations, use the [US Government Support Portal requests link](https://federal-support.gitlab.com/hc/en-us/requests)
1. Click the `Requests I'm CC'd on` link towards the top-left of the screen:
  - Individuals, business and enterprise customers, use the [Global Support Portal CC'd requests link](https://support.gitlab.com/hc/en-us/requests/ccd)
  - For US Government pre-approved organizations, use the [US Government Support Portal CC'd requests link](https://federal-support.gitlab.com/hc/en-us/requests/ccd)

### View your organization's tickets

If your organization is utilizing the [Shared Organizations]({{LINK: managing-support-contacts.md#shared-organizations}}) feature, you can view the tickets submitting by your organization by doing the following:

1. [Log into your account]({{LINK: portal.md#logging-into-your-support-portal-account}})
1. Click your profile link in the top right of the page
1. Click the Requests link:
  - Individuals, business and enterprise customers, use the [Global Support Portal requests link](https://support.gitlab.com/hc/en-us/requests)
  - For US Government pre-approved organizations, use the [US Government Support Portal requests link](https://federal-support.gitlab.com/hc/en-us/requests)
1. Click the `Organization requests` link towards the top-left of the screen:
  - Individuals, business and enterprise customers, use the [Global Support Portal organization requests link](https://support.gitlab.com/hc/en-us/requests/organization)
  - For US Government pre-approved organizations, use the [US Government Support Portal organization requests link](https://federal-support.gitlab.com/hc/en-us/requests/organization)

**NOTE** If you do not see that option, it would mean your organization is not utilizing the [Shared Organizations]({{LINK: managing-support-contacts.md#shared-organizations}}) feature.

#### Getting notified about your organization's tickets

If you wish to be notified about your organization's tickets (and your organization is using the [Shared Organizations]({{LINK: managing-support-contacts.md#shared-organizations}}) feature setup, you can click the `Follow` button to the right of the search bar on the [Organization requests page]({{LINK: portal.md#view-your-organizations-tickets}}).

To turn this off, simple click the `Unfollow` button to the right of the search bar on the [Organization requests page]({{LINK: portal.md#view-your-organizations-tickets}}).

### Updating a ticket

There are currently two ways to update a non-closed ticket:

1. Via email, which is done by replying to the latest notification email you received for the ticket.
1. Via the support portal, which is done by navigating to the ticket in question via the support portal, going to the very bottom of the page, clicking the `Add to conversation` field, typing out your response, and then clicking the `Submit` button.

#### Adding additional participants (CCs) to your ticket 

Please note the following:

- This requires logging into the support portal to do. It cannot be done via email. Requests to add additional participants made through the ticket interaction will not be actioned on.
- This functionality is not available on the support portal for US Government pre-approved organizations.

When updating a ticket, you can add additional participants (CCs) on your ticket. This will ensure the CC'd person gets update notifications for the ticket (and can also reply on said ticket).

To do this, you will have to post an update on the ticket itself (meaning a comment must be made). Right above the comment text box is the CC box, which will allow you to type out email addresses to be CC'd on the ticket. To enter the email, the system waits for a space delimeter. So add the emails of a@abc.com, b@abc.com, and c@abc.com, you would enter

`a@abc.com b@abc.com c@abc.com`

into the field. You can remove an email before you submit the update by clicking the `x` to the right of the email address.

Once a CC is added, it cannot be removed via the support portal. You will need to make a comment on the ticket requesting the GitLab agent completely remove the email as a CC on the ticket.

### Creating a follow-up ticket

To create a follow-up ticket for a previously closed ticket, navigate to the ticket in question, scroll to the last comment on the ticket, and click the `create a follow-up` link. This will send you to the ticket creation page, copying over the fields from the original ticket. It will also pre-populate the ticket's description with a message about it being a follow-up to the original ticket.

### Solving a ticket

In very specific situations, you are able to mark your ticket as solved. To be able to do this:

- The ticket must be assigned to a GitLab Agent
- The ticket's type may not be problem
- You must be the requester on the ticket (i.e. you cannot mark other members of your organization's tickets as solved).

If all of these criteria are hit, you can mark the ticket as solved. To do this:

1. Navigate the to ticket in question
1. Go to the bottom of the page for your ticket
1. Click the `Mark as solved` button

If you wish to mark the ticket as solved and update the ticket with a reply, you would type out your reply in the text box (bottom of the ticket) and then click the `Mark as solved & Submit` button.

## Information on ticket flow

When it comes to the flow of your ticket, there are some things to keep in mind.

### We use auto-responders for efficiency 

Where possible, for the sake of efficiency, we use auto-responders from a bot account we control. The goal of these is to try to solve common issues that can be easily solved with a quick reply.

We utilize these to help ensure the fastest resolution to your tickets whenever possible.

### We automatically modify the ticket status based on unresponsiveness timelines 

To ensure tickets do not stay in a pending (awaiting your response) forever, we have setup automated triggers to change the status based on unresponsiveness timelines:

- If a ticket is pending (awaiting your response) for 7 days, a reminder notification is sent on your ticket reminding you of the current state of the ticket.
- If a ticket is pending (awaiting your response) for 14 days, we change the ticket's status to solved (and send out a notification email). At this point, you can still update the ticket to get it moved out of the solved state.
- If a ticket is in a solved state for 7 days, we automatically close the ticket. At this point, the ticket can no longer be updated. The only way to resume a conversation about the ticket is to [create a follow-up ticket]({{LINK: portal.md#creating-a-follow-up-ticket}}).

### Ticket responses can be in other languages

**NOTE** This functionality is not available on the support portal for US Government pre-approved organizations.

Ticket support is available in the following languages:

- Chinese (Simplified)
- English
- French
- German
- Italian
- Japanese
- Korean
- Portuguese
- Spanish

While we do not currently provide translated interfaces for our support portals, you can write in your preferred language and we will respond accordingly.

Should you be offered a call, only English is available.

**NOTE** Any attached media used for ticket resolution must be sent in English.

## Support Portal Troubleshooting

Occasionally, you may find the Support Portal not acting as expected. As the system relies pretty heavily on cookies and browser sessions, we recommend taking the following course of action:

1. Ensure your browser is allowing third party cookies. These are often vital for the system to work. A general list to allow would be:
   - `[*.]zendesk.com`
   - `[*.]zdassets.com`
   - `[*.]gitlab.com`
1. Disable all plugins/extensions/addons on the browser.
1. Disable any themes on the browser.
1. Clear all cookies and cache on the browser.
1. Try logging in again to the the Support Portal.
1. If you are still having issues, obtain the following:
   - the browser’s version
   - the browser's type
   - your operating system (and distro)
   - any other identifying information
   - the complete contents of your Javascript console for your browser
1. Send all of that to support. If you are unable to create a ticket, then communicate with your Account Manager. The next best place to send the information is via a GitLab.com issue.
