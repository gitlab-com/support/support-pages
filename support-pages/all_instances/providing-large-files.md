# Support - Providing Large Files

Zendesk has a [maximum attachment size](https://support.zendesk.com/hc/en-us/articles/4408832757146#topic_lv2_cnx_xdb) of 50MB per file. Zendesk will not allow us to increase this limit any further.

## Alternatives

### Compression

If you're sending a text file or an archive with mostly text files, then compress it. Use either bz2 (preferred) or gzip (faster) compression. Zip compression is fine if you're on Windows.
If this brings your file under the 50MB limit, attach the archive to the Zendesk ticket.

### File sharing services

You can use your own choice of file sharing service. Be aware submitting potentially sensitive data to 3rd parties carries a risk. Check with your security team for a properly vetted choice.

### GitLab private project

1. [Sign up for a gitlab.com account](https://gitlab.com/users/sign_in#register-pane) if you don't already have one. 
1. Create a private project and upload your files there.
1. Ask the Support Engineer(s) assisting you to provide you with their GitLab Handle.
1. Invite the Support Engineer(s) to the project with the **Reporter** permission.

### Split with GNU split

Because the Zendesk size limit is per file, you can split a large file and attach them to the Zendesk ticket.

The `split` command is bundled in GNU coreutils, which should be installed on all Unix-like operating systems by default. Avoid using alternatives like winzip, winrar, 7zip, etc. 

We've included an example below:

> `split -b <size> <source file> <prefix for new files>`
>
> `split -b 19M source-file.tar.bz2 "target-file.tar.bz2."s`

This creates files such as `target-file.tar.bz2.aa` in your current directory. These files can be later joined with the `cat` command.

> `cat target-file.tar.bz2.* > joined-file.tar.bz2`
