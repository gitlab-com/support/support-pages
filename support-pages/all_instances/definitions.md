# Support - Definitions

## Definitions of Support Impact

The SLA times listed are the time frames in which you can expect the first response.

### Severity 4 | Low

Questions or Clarifications around features or documentation or deployments (24 hours) Minimal or no Business Impact. Information, an enhancement, or documentation clarification is requested, but there is no impact on the operation of GitLab. Implementation or production use of GitLab is continuing and work is not impeded. Example: A question about enabling ElasticSearch.

### Severity 3 | Normal

Something is preventing normal GitLab operation (8 hours) Some Business Impact. Important GitLab features are unavailable or somewhat slowed, but a workaround is available. GitLab use has a minor loss of operational functionality, regardless of the environment or usage. Example: A known bug impacts the use of GitLab, but a workaround is successfully being used as a temporary solution.

### Severity 2 | High

GitLab is Highly Degraded (4 hours) Significant Business Impact. Important GitLab features are unavailable or extremely slowed, with no acceptable workaround. Implementation or production use of GitLab is continuing; however, there is a serious impact on productivity. Example: CI Builds are erroring and not completing successfully, and the software release process is significantly affected.

### Severity 1 | Urgent

Your instance of GitLab is unavailable or completely unusable (30 Minutes) A GitLab server or cluster in production is not available, or is otherwise unusable. An emergency ticket can be filed and our On-Call Support Engineer will respond within 30 minutes. Example: GitLab showing 502 errors for all users.

### Ticket severity and customer priority

When submitting a ticket to support, you will be asked to select both a severity and a priority. The severity will display definitions aligning with [the above section](#definitions-of-support-impact). Priority is lacking any definition - this is because the priority is whatever you and your organization define it as. The customer priority (shorted on the forms as priority) is Support's way of asking the impact or importance the ticket has to your organization and its needs (business, technical, etc.).

**Note:** The [US Government support portal](https://federal-support.gitlab.com/) is often setup differently than the [Global support portal](https://support.gitlab.com/) and might differ in the questions asked via the ticket submission forms.

## Definition of Scaled Architecture

Scaled architecture is defined as any GitLab installation that separates services for the purposes of resilience, redundancy or scale. As a guide, our [2,000 User Reference Architecture](https://docs.gitlab.com/ee/administration/reference_architectures/2k_users.html) (and higher) would fall under this category.

Priority Support is required to receive assistance in troubleshooting a scaled implementation of GitLab.

In Omnibus and Source installations, Scaled Architecture is any deployment with multiple GitLab application nodes. This does not include external services such as Amazon RDS or ElastiCache.

| Service  | Scaled Architecture  |  Not Scaled Architecture |
|---|---|---|
| Application (GitLab Rails)  |  Using multiple application nodes to provide resilience, scalability or availability | Using a single application node  |
| Database  | Using multiple database servers with Consul and PgBouncer | Using a single separate database or managed database service  |
| Caching  | Using Redis HA across multiple servers with Sentinel  |  Using a single separate server for Redis or a managed Redis service |
| Repository / Object Storage | Using one or more separate Gitaly nodes. Using NFS across multiple application servers.  |  Storing objects in S3 |
| Job Processing | Using one or more separate Sidekiq nodes  |  Using Sidekiq on the same host as a single application node |
| Load Balancing  | Using a Load Balancer to balance connections between multiple application nodes  |  Using a single application node |
| Monitoring  | Not considered in the definition of Scaled Architecture  | Not considered in the definition of Scaled Architecture  |

## Support Email Domains

As a convenience, [GitLab's Support Portal](https://support.gitlab.com) allows users to collaborate on raised tickets via email. If your company uses allowlists for managing incoming email, you may receive email from GitLab Support from the following domains:

- `support.gitlab.com`
- `gitlab.zendesk.com`
- `gitlab.com`

## Common Terms

 - Self-managed means GitLab Omnibus, Docker and Cloud Native distributions running on customer provided resources. GitLab Enterprise Edition and Community Edition both can be self-managed.
 - SaaS refers to both of GitLab's Software-as-a-service products: GitLab.com and GitLab Dedicated.
 - GitLab.com is GitLab's multi-tenant SaaS offering.
 - GitLab Dedicated is GitLab's single-tenant SaaS offering.
