# Support - US Government Support

US Government Support is built for companies and organizations that require that only US Citizens have access to the data contained within their support issues. The unique requirements of US Government Support result in the following specific policies:

## Limitations

To be eligible to utilize this instance you must be a public sector organization and meet either of the following criteria:

- Be a current active subscriber to one of the following product offerings:
   - US Govt Ultimate with 24x7 US Citizen Support
   - US Govt Ultimate with 12x5 US Citizen Support
   - US Govt Premium with 12x5 US Citizen Support
- Be considered a legacy US Government support customer

For more information about utilizing this method of support, please contact your Account Manager.

## Hours of operation

#### For customers with 12x5 US Citizen support subscriptions or legacy US Government customers
- Monday through Friday, 0500-1700 Pacific Time
- Weekday [Upgrade Assistance]({{LINK: scheduling-upgrade-assistance.md}}) requests for the US Government Support team may be scheduled within the US Government support hours of operation listed above. All [information]({{LINK: scheduling-upgrade-assistance.md#what-information-do-i-need-to-schedule-upgrade-assistance}}) relating to the request must be provided at least 1 week in advance.

#### For customers with 24x7 US Citizen support subscriptions

- For cases that fall under [severity 3]({{LINK: definitions.md#severity-3}}) (normal) and [severity 1]({{LINK: definitions.md#severity-4}}) (low) definitions the 12x5 hours listed above apply
- For cases that align with [severity 1]({{LINK: definitions.md#severity-1}}) (emergency) and [severity 2]({{LINK: definitions.md#severity-2}}) (high) definitions assistance is available 24x7

## US Government Emergency support

The US Government Support instance offers emergency support to customers experiencing a [severity 1]({{LINK: definitions.md#severity-1}}) issue in their production environments.

Emergencies can be filed either via the email address you should have received from your Account Manager or via the [Emergency support request form](https://federal-support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001421112).

## CCs are disabled

To help ensure that non-US citizens are not inadvertently included in a support interaction, the "CC" feature is disabled. As a result, support personnel will be unable to add additional contacts to support tickets.

Authorized support users may opt to include other contacts in support cases through the setup of a [shared organization]({{LINK: managing-support-contacts.md#shared-organizations}}) within the US Federal support portal.

## Discovery Session Requests

The US Government support team provides users an opportunity to request a discovery session at the start of a new technical support case by selecting the "Discovery Session Request" option.

Discovery requests are granted at the discretion of the assigned support engineer.

The session is a brief opportunity to augment information provided when initiating a case and to work with a support engineer to collect relevant information such as screenshots, log files, replication steps, etc.

**The process**

When a discovery session request is made, the assigned engineer will review the case description and provide a link to schedule a session for a future date. The engineer may also request additional information asynchronously prior to sending a link to schedule the call if sufficient information hasn't yet been provided. When a session is scheduled but has not yet occurred troubleshooting can continue asynchronously via the case. In the event a resolution is found the engineer may opt to cancel the discovery session. After the scheduled session has occured the engineer will follow-up in the case with a synopsis of what was discussed and prompt to have the items collected uploaded to the case for further review.
Troubleshooting will typically be done asynchronously from this point forward.

**When is a discovery session not appropriate?**

- In the event of a [Severity 1 incident]({{LINK: definitions.md#severity-1}}) the discovery session request should not be used. Instead you must follow the process to [file an emergency request]({{LINK: us-government-support.md#us-government-emergency-support}}).
- Walkthroughs of documentation and engineer ride-along while attempting to setup a feature is not within the scope of a discovery session and may be declined in favor of asynchronous troubleshooting.
