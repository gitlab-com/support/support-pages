<!--
Use when:
- Documenting solutions for specific errors or problems
- Addressing system failures or malfunctions
- Providing troubleshooting steps for known issues
- Explaining how to recover from common failures

For a more detailed style guide see https://handbook.gitlab.com/handbook/support/knowledge-base/kb-style-guide/ 
-->

# CI/CD job fails with error: `argument list too long`

<!--
The title should:
- Clearly describe the error or problem
- Include specific error messages or codes if applicable
- Be searchable and SEO-friendly
- Focus on the symptom rather than the solution
Only capitalize the first word and proper nouns.
Examples:
- Repository size exceeds quota limit
- LDAP authentication fails after upgrade
-->

## Description

<!-- 
- Describe the symptoms customers will see when they encounter an issue.
- One bullet point per item. Use a bullet point even if there's only one symptom.
- List symptoms in order of significance (most impactful first)
- Include exact error messages in blockquotes
- Include specific log entries or error codes
- Note any impact on functionality or data
-->

- CI/CD job fails during the environment prepare phase with error:

    ```bash
    exec /usr/bin/dumb-init: argument list too long 
    ```

## Environment

<!-- 
Describe the environment configuration in which the issue applies.
Examples:
- SSO enabled
- Kubernetes cluster
-->

CI/CD jobs running on Linux systems (Example: job running in a container
created from alpine:latest image)

- **Impacted offerings:**
  - GitLab.com
  - GitLab Dedicated
  - GitLab Self-Managed

<!--
Remove if all known / supported versions are affected 
Examples:
- 16.1 to 16.3
- 17.1 and later
- 16.4 and earlier
- 17.x
-->

## Solution

<!-- 
1. Provide clear steps to resolve the issue
2. Start each step with an action verb
3. Include commands in code blocks with comments
4. Note any required permissions or prerequisites
5. Mention any service restarts or system impacts

For multiple resolution paths:
- Option A: Steps for scenario 1
- Option B: Steps for scenario 2

If providing a temporary fix, place it under a new ### Workaround heading
-->

Reduce the size of the CI/CD variable value so that it doesn't exceed 128 KiB.

### Workarounds

- Move large variables to files where possible.
- If a single variable is too large, try using [project-level secure files](https://docs.gitlab.com/ee/ci/secure_files/) or bring the file to the job through some other mechanism.

## Cause

<!-- 
- Explain what caused the issue and why the problem occurs
- Reference any known bugs, issues, or changes
- Note any design limitations or constraints

Use technical details when relevant but keep explanations clear
-->

Linux systems have a size limit for any single environment variable value. This is set via the `MAX_ARG_STRLEN` [definiton](https://github.com/torvalds/linux/blob/master/include/uapi/linux/binfmts.h#L15) during compilation of the kernel. This value generally ends up being 131072 bytes (128 KiB).

This includes the [pre-defined CI/CD variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html#predefined-variables) which are passed to the job and in particular the `CI_COMMIT_MESSAGE` variable which can be long.

## Related Links

<!-- 
Add links to relevant resources such as GitLab issues or articles. Example format:
[Related topics](https://docs.gitlab.com/ee/development/documentation/topic_types/index.html#related-topics)
-->

- [Feature Request: Provide a method to overwrite/disable predefined CI variables before being sent to the runner](https://gitlab.com/gitlab-org/gitlab/-/issues/431626)
- [Issue: "argument list too long" error with very large CI/CD variable](https://gitlab.com/gitlab-org/gitlab/-/issues/392406)
