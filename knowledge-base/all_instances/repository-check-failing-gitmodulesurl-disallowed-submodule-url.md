
# Encountering `gitmodulesUrl: disallowed submodule url` error when running repository check

## Description

- Repo check reports: `error in blob xxxx: gitmodulesUrl: disallowed submodule url: ../..//xxxxx/xxxxxx.git`

## Environment

Issue can potentially be reported in a Self-Managed environment running 17.0+ during a repository check.

- **Impacted offerings:**
  - GitLab Self-Managed

- **Impacted versions:**
  - 17.x to current

## Cause

This is a [Known Issue](https://gitlab.com/gitlab-org/gitlab/-/issues/468560) from 17.x as a result of more strict repository consistency checks being enforced. This new behavior comes from a change in upstream Git, whereby this check was added. It is therefore not a Gitaly issue specifically.

## Solution

The recommended solution is to resolve the .gitmodules inconsistency that's causing the error, as mentioned in this [issue](https://gitlab.com/gitlab-org/gitlab/-/issues/468560). Git supports ignoring objects to be checked using the .git-fsck-skiplist configuration. The following should allow the fsck check to pass without complaints.

Here's an example of how to do it:
```
$ cat <<EOF >.git-fsck-skiplist
> # invalid submodule path
> 19a97d3b70760c74b780c8134e33f5392292c2e6
> EOF
$ git config fsck.skipList .git-fsck-skiplist
```
 
### Workaround

Administrators can [override repository consistency checks](https://docs.gitlab.com/ee/administration/gitaly/consistency_checks.html#override-repository-consistency-checks)
## Additional information
 
The change was added to the git fsck checks, owned and managed by Git. GitLab and the Gitaly team could override the default checks. [Gitaly issue 5641](https://gitlab.com/gitlab-org/gitaly/-/issues/5641) was opened to re-evaluate what default checks GitLab disables in Analyze exceptions to the `git fsck` command. 

## Related links

Reference 2 other stackoverflow posts on how to actually correct the git history to exclude the offending blob:
- Method #1: [How to fix 'gitmodulesPath: disallowed submodule path' on 'git fsck'](https://stackoverflow.com/questions/55569644/how-to-fix-gitmodulespath-disallowed-submodule-path-on-git-fsck)
- Method #2: [Building the replacement commit](https://stackoverflow.com/questions/55858440/how-do-i-fix-a-git-fsk-error-due-to-old-invalid-gitmodules-file)
