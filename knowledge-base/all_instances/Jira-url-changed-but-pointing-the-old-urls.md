# Updating Jira site URL does not change existing Jira issue references

## Description

After updating the Jira URL, the existing links still point to the old URL.

## Environment

Jira integration is enabled

- **Impacted offerings:**
  - GitLab.com
  - GitLab Dedicated
  - GitLab Self-Managed

## Solution

**GitLab Dedicated and Self-Managed**

[Invalidate the existing markdown cache](https://docs.gitlab.com/administration/invalidate_markdown_cache/#invalidate-the-cache) using either the API or Rails console.

**GitLab.com**

Contact Support to invalidate the cache.

## Cause

For performance reasons, GitLab caches the HTML version of Markdown text. These cached versions can become outdated, such as when the external_url configuration option is changed.

