# 403 error blocks project creation from template when Pure Storage is configured

## Description

- `Error importing repository into (projectName) - unsupported response downloading fragment 403` error
  when creating new project from template  
- Pure Storage S3-compatible Object Store configured as object storage.
- Other operations involving object storage working as expected.

## Environment

- Pure Storage configured for GitLab object storage

**Impacted offerings:**

- GitLab Self-Managed

## Solution

Set `'aws_signature_version' => 2` in the [object storage connection settings](https://docs.gitlab.com/administration/object_storage/#amazon-s3).

## Cause

The default `aws_signature_version` value of `4` exposes an incompatibility between Pure Storage Object Store and
the [library used by GitLab](https://gitlab.com/gitlab-org/gitlab/-/blob/v17.9.0-ee/lib/gitlab/import_export/command_line_util.rb#L82)
to download the project template export file for import into the new project. Different libraries are used for other object storage operations
and these do not experience the same issue.