<!--
Use when:
- Documenting solutions for specific errors or problems
- Addressing system failures or malfunctions
- Providing troubleshooting steps for known issues
- Explaining how to recover from common failures

For a more detailed style guide see https://handbook.gitlab.com/handbook/support/knowledge-base/kb-style-guide/ 
-->

# GitLab redirects to another page, or the old server, when restored to a new server
<!--
The title should:
- Clearly describe the error or problem
- Include specific error messages or codes if applicable
- Be searchable and SEO-friendly
- Focus on the symptom rather than the solution
Only capitalize the first word and proper nouns.
Examples:
- Repository size exceeds quota limit
- LDAP authentication fails after upgrade
-->

## Description

<!-- 
- Describe the symptoms customers will see when they encounter an issue.
- One bullet point per item. Use a bullet point even if there's only one symptom.
- List symptoms in order of significance (most impactful first)
- Include exact error messages in blockquotes
- Include specific log entries or error codes
- Note any impact on functionality or data
-->

- After restoring a GitLab backup to a new server, as part of [migrating to a new server](https://docs.gitlab.com/ee/administration/backup_restore/migrate_to_new_server.html), attempts to load the new server's login page are redirected elsewhere.
- This can happen even after changing `external_url` in `gitlab.rb` and reconfiguring
- Visiting the restored site with `curl -v`shows HTTP 302 redirects

## Environment

<!-- 
Describe the environment configuration in which the issue applies.
Examples:
- SSO enabled
- Kubernetes cluster
-->

- [Sign-in information](https://docs.gitlab.com/ee/administration/settings/sign_in_restrictions.html#sign-in-information) is set on source system, and therefore it is also set on restored system

- **Impacted offerings:**
  - GitLab Self-Managed

## Solution

<!-- 
1. Provide clear steps to resolve the issue
2. Start each step with an action verb
3. Include commands in code blocks with comments
4. Note any required permissions or prerequisites
5. Mention any service restarts or system impacts

For multiple resolution paths:
- Option A: Steps for scenario 1
- Option B: Steps for scenario 2

If providing a temporary fix, place it under a new ### Workaround heading
-->

Since you cannot log into the restored system to clear the setting, this must be done from the GitLab [rails console](https://docs.gitlab.com/ee/administration/operations/rails_console.html).

1. Connect to the Rails console of your restored GitLab installation:
    - Linux:
        ```shell
        sudo gitlab-rails console
        ```
    - Kubernetes:
        ```shell
        kubectl exec -it <toolbox-pod-name> -- gitlab-rails console
        ```
    - Docker:
        ```shell
        docker exec -it <rails-container-id> gitlab-rails console
        ```
1. Confirm that the **Home Path URL** is the same as the URL that visitors are 302 redirected to:
    ```ruby
    pp ApplicationSetting.current.home_path_url
    ```
1. Clear the **Home Path URL** application setting:
    ```ruby
    ApplicationSetting.current.update!(home_path_url: '')
    ```
1. Exit the Rails console by typing `exit`

You may now open a web browser to the new server's `external_url`, and it will no longer redirect you.

## Cause

<!-- 
- Explain what caused the issue and why the problem occurs
- Reference any known bugs, issues, or changes
- Note any design limitations or constraints

Use technical details when relevant but keep explanations clear
-->

If the system that is being migrated has [Sign-in information](https://docs.gitlab.com/ee/administration/settings/sign_in_restrictions.html#sign-in-information) set in it's Sign-in Restrictions, then this is restored along with all the other application settings. All users that are not logged in are redirected to the page represented by the configured **Home page URL**, if value is not empty.

## Additional Information

Example redirect output from curl when broken:

```
~► curl -v http://34.127.83.216
*   Trying 34.127.83.216:80...
* Connected to 34.127.83.216 (34.127.83.216) port 80
> GET / HTTP/1.1
> Host: 34.127.83.216
> User-Agent: curl/8.7.1
> Accept: */*
>
* Request completely sent off
< HTTP/1.1 302 Found
< Server: nginx
< Date: Wed, 22 Jan 2025 00:30:33 GMT
< Content-Type: text/html; charset=utf-8
< Transfer-Encoding: chunked
< Connection: keep-alive
< Cache-Control: no-cache
< Content-Security-Policy:
< Location: https://example.com/wombat
< Permissions-Policy: interest-cohort=()
< X-Content-Type-Options: nosniff
< X-Download-Options: noopen
< X-Frame-Options: SAMEORIGIN
< X-Gitlab-Meta: {"correlation_id":"01JJ5PP0SNVC74WDH8FAE91SS1","version":"1"}
< X-Permitted-Cross-Domain-Policies: none
< X-Request-Id: 01JJ5PP0SNVC74WDH8FAE91SS1
< X-Runtime: 0.037282
< X-Ua-Compatible: IE=edge
< X-Xss-Protection: 1; mode=block
< Strict-Transport-Security: max-age=63072000
< Referrer-Policy: strict-origin-when-cross-origin
<
* Connection #0 to host 34.127.83.216 left intact
<html><body>You are being <a href="https://example.com/wombat">redirected</a>.</body></html>
```

## Related Links

- [Migrating to a new server](https://docs.gitlab.com/ee/administration/backup_restore/migrate_to_new_server.html)
- [Configuring the external URL for GitLab](https://docs.gitlab.com/omnibus/settings/configuration.html#configure-the-external-url-for-gitlab): 
- [Customize your sign-in and register pages](https://docs.gitlab.com/ee/administration/appearance.html#customize-your-sign-in-and-register-pages)
- [Custom **Home page URL** or **After sign-out path**](https://docs.gitlab.com/ee/administration/settings/sign_in_restrictions.html#sign-in-information)

