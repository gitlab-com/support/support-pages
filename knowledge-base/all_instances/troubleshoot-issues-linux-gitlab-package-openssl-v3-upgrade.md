# Troubleshoot and address issues after GitLab Linux package upgraded to OpenSSL v3

## Issue

After upgrading to GitLab `17.7` (or `17.3.0`, `17.3.1`), customers might notice that outgoing or incoming TLS connections fail with a message like:

- `no protocols available`
- `certificate key too weak`

Since all components shipped with the Linux package are compatible with OpenSSL 3, folks will notice this problem with services and integrations that are 'external' and not part of the GitLab package. Examples of services or integrations where folks are likely to notice this problem include but are not limited to:

- [LDAP](https://docs.gitlab.com/ee/administration/auth/ldap/)
- [Webhooks](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html)

## Cause

In GitLab `17.7`, the Linux package [upgrades](https://docs.gitlab.com/ee/update/versions/gitlab_17_changes.html#openssl-3-upgrade) OpenSSL from `v1.1.1w` to `v3.0.0`.
This change also happened in `17.3.0` and `17.3.1` but was reverted in `17.3.2`.

### Impacted Environments

The change discussed in this article that happens in `17.7` is limited to the Omnibus Linux package. Cloud Native GitLab (CNG) already upgraded to OpenSSL 3 in GitLab 16.7.0.

The change discussed in this article can also happen to folks who upgrade to `17.3.0` or `17.3.1`.

With this change, GitLab will require TLS 1.2 or higher for all outgoing and incoming TLS connections. Since TLS 1.0 and 1.1 have reached end-of-life and are no long considered secure, they should no longer be used.

### Determining Impact

The `ssl-enum-ciphers` [script](https://nmap.org/nsedoc/scripts/ssl-enum-ciphers.html) that's built into `nmap` can be useful to checking whether the service or integration in question meets the requirements. To check whether an example LDAP server is configured properly, you would use a command like this (adjusting `389` to a different port as necessary and adjusting `ldap.example.com` to the correct hostname):

```
nmap -sV --script ssl-enum-ciphers -p 389 ldap.example.com
``` 

More information about [assessing compatibility with OpenSSL 3](https://docs.gitlab.com/omnibus/settings/ssl/openssl_3.html#assessing-compatibility-with-openssl-3) is available in the docs.

## Resolution

### Before upgrade to 17.7, adjust external services and integrations to use TLS 1.2 (or higher) and enough bits of security

Typically, the best path forward is to adjust the external services and integrations to ensure that they are meeting the requirements we lay out in the docs on [TLS support](https://docs.gitlab.com/ee/security/tls_support.html) in GitLab. Since this requires adjusting non-GitLab components, the GitLab Support team is limited in our ability to assist with adjusting these external services and integrations.

### Upgrade to 17.3.2 or later (temporary)

For folks who notice this upon upgrade to `17.3`, it is possible to temporarily resolve this problem by upgrading to `17.3.2` or later. (It's recommended to use the latest patch release that is available.) This resolution will be temporary as the problem will recur upon upgrade to `17.7`. The long-term approach is to adjust the external services and integrations to meet the [requirements](https://docs.gitlab.com/ee/update/versions/gitlab_17_changes.html#openssl-3-upgrade).

### Remain on 17.6

If it's not possible to immediately adjust these external services and integrations, remain on `17.6` until you can adjust them. 

## Related Links

- [OpenSSL 3 upgrade](https://docs.gitlab.com/ee/update/versions/gitlab_17_changes.html#openssl-3-upgrade)
- [TLS support](https://docs.gitlab.com/ee/security/tls_support.html)
- [Troubleshooting SSL](https://docs.gitlab.com/omnibus/settings/ssl/ssl_troubleshooting.html)
- [Details on how GitLab and SSL work](https://docs.gitlab.com/omnibus/settings/ssl/#details-on-how-gitlab-and-ssl-work)
- [Useful OpenSSL debugging commands](https://docs.gitlab.com/omnibus/settings/ssl/ssl_troubleshooting.html#useful-openssl-debugging-commands)
- [GitLab Linux package being upgraded to OpenSSL 3 in GitLab 17.7](https://about.gitlab.com/blog/2024/09/18/gitlab-linux-package-being-upgraded-to-openssl-3/)
- [Support for TLS 1.0 and 1.1 discontinued on GitLab.com and GitLab API on 2018-12-15](https://about.gitlab.com/blog/2018/12/17/gitlab-tls1011-discontinued-update/)
