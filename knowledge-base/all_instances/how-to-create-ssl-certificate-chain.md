<!-- This template is meant to be used for specific issues with specific resolution steps. -->

# How to create and install a complete SSL certificate chain in GitLab
<!-- Use sentence-case capitalization for the title. -->

## Description

GitLab requires a complete certificate chain to establish trust between the server and clients. You might need to build a full chain certificate under the following situations:

- You are setting up a new GitLab instance and want to add an SSL certificate
- You need to rotate an expiring SSL certificate in an existing GitLab instance

If using SSL certificates from untrusted or unrecognized certificate authorities (CAs) you might see:

- Security warnings in web browsers when accessing the GitLab user interface.
- Certificate-related errors during Git operations over HTTPS.
- [Common SSL errors](https://docs.gitlab.com/omnibus/settings/ssl/ssl_troubleshooting.html#common-ssl-errors) listed in our documentation.

## Environment

- **Impacted offerings:**
  - GitLab Self-Managed

## Prerequisites

SSL certificate files from your Certificate Authority (CA):

- Domain certificate (`domain.crt`)
- Private key (`domain.key`)
- Intermediate certificate(s) (`intermediate.crt`)
- Root certificate (`root.crt`)

Some CAs may combine the intermediate/root certificates into a single file like `chain.crt` or similar.

All certificates must be in PEM format. PEM-encoded certificates can use '.crt' or '.pem' file extensions. However, the file content, not the extension, determines the format. To verify format:

1. Open the certificate file in a text editor.
1. Confirm it starts with `-----BEGIN CERTIFICATE-----`.

## Resolution

1. Combine the certificates into one file in the following order:
   - Domain certificate (your site's certificate)
   - Intermediate certificate(s) (if you have them - there might be multiple)
   - Root certificate
   
   **Note:**
   GitLab will automatically use your certificate without additional configuration 
   if the filename matches the `external_url` set in `/etc/gitlab/gitlab.rb`. 
   For example, if your `external_url` is `'https://gitlab.example.com'`, name your certificate `gitlab.example.com.crt`. 
   To use a different naming convention, review the [configure HTTPS manually](https://docs.gitlab.com/omnibus/settings/ssl/index.html#configure-https-manually) docs.

   ```shell
   # If your certificates are all in separate files
   cat domain.crt intermediate.crt root.crt > gitlab.example.com.crt

   # If your CA has provided you with a chain already
   cat domain.crt chain.crt > gitlab.example.com.crt
   ```

   The resulting `gitlab.example.com.crt` file should look like this:

   ```plaintext
   -----BEGIN CERTIFICATE-----
   Your Domain certificate (or main SSL certificate): domain.crt
   -----END CERTIFICATE-----
   
   -----BEGIN CERTIFICATE-----
   Your Intermediate certificate: intermediate.crt
   -----END CERTIFICATE-----

   -----BEGIN CERTIFICATE-----
   Your Root certificate: root.crt
   -----END CERTIFICATE-----
   ```

1. Verify the full chain certificate:

   1. To view the certificate chain from top to bottom, run:
    
      ```shell
      openssl crl2pkcs7 -nocrl -certfile gitlab.example.com.crt | openssl pkcs7 -print_certs -noout | grep -E "subject=|issuer="
      ```
    
   1. Confirm the chain output 
        
      - The top certificate `subject` should match the domain name of the GitLab instance (`gitlab.example.com`)
      - The `issuer` should match the next certificate's `subject` (`DigiCert Intermediate CA`). Confirm this for each certificate in the chain
      - The chain ends with the root certificate, where subject and issuer are identical (`DigiCert Root CA Inc`)

      ```
      # Example output

      # Domain Certificate (Your GitLab instance certificate)
      subject=CN = gitlab.example.com
      issuer=CN = DigiCert Intermediate CA

      # Intermediate Certificate
      subject=CN = DigiCert Intermediate CA
      issuer=CN = DigiCert Root CA Inc

      # Root Certificate
      subject=CN = DigiCert Root CA Inc
      issuer=CN = DigiCert Root CA Inc 
      ```

1. Copy the certificate chain and private key to `/etc/gitlab/ssl`:

   ```shell
   sudo cp gitlab.example.com.crt /etc/gitlab/ssl/
   sudo cp domain.key /etc/gitlab/ssl/gitlab.example.com.key
   ```

1. Set the correct permissions:

   ```shell
   sudo chmod 600 /etc/gitlab/ssl/gitlab.example.com.*
   sudo chown root:root /etc/gitlab/ssl/gitlab.example.com.*
   ```

1. [Reconfigure](https://docs.gitlab.com/ee/administration/restart_gitlab.html#reconfigure-a-linux-package-installation) and [restart NGINX](https://docs.gitlab.com/ee/administration/restart_gitlab.html#restart-a-linux-package-installation):

   ```shell
   sudo gitlab-ctl reconfigure
   sudo gitlab-ctl restart nginx

   # If you are updating an existing SSL certificate, you can reload NGINX instead
   sudo gitlab-ctl hup nginx
   ```

1. Access your GitLab instance via your browser to confirm there are no certificate warnings.

## Related links

- [Configure HTTPS manually](https://docs.gitlab.com/omnibus/settings/ssl/index.html#configure-https-manually)
- [Troubleshooting SSL](https://docs.gitlab.com/omnibus/settings/ssl/ssl_troubleshooting.html)