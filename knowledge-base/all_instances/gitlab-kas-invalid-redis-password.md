# GitLab-kas pod fails to start due to Redis authentication error

## Description

- The gitlab-kas pod fails to start in a Kubernetes cluster.
- Users observe the following error message in the pod logs:

  ```text
  Program aborted: redis client: WRONGPASS invalid username-password pair or user is disabled.
  ```

- This error occurs even when the Redis password is correctly set in the configuration.

## Environment
 
- Primarily affects GitLab instances installed via Helm chart

- **Impacted offerings:**
  - GitLab Self-Managed

## Solution

1. Check the Redis password secret for any unintended newline characters:

   ```bash
   ## Change redis-password with the key used for the secret
   kubectl get secret <redis-secret-name> -o jsonpath='{.data.redis-password}' | base64 --decode
   ```
1. If a newline is present, create a new secret without the newline:

   ```bash
   kubectl create secret generic <new-redis-secret-name> --from-literal=redis-password='your-redis-password'
   ```
1. Update your Helm values file to use the new secret:

   ```yaml
   global:
     redis:
       auth:
         secret: <new-redis-secret-name>
         key: redis-password
   ```
1. Upgrade your GitLab installation with the new values:

   ```
   helm upgrade --install gitlab gitlab/gitlab -f <path_to_your_values_file>
   ```
1. Wait for the `gitlab-kas` pod to restart and verify that it starts successfully.

## Cause

This issue occurs when the Redis password secret contains an unintended newline character. The gitlab-kas component reads this password including the newline, causing authentication failures when connecting to Redis, even though the password itself is correct.

## Related Links

- [Trim new lines when reading password_file for Redis password](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/issues/669)