
# `The member's email address is not linked to a SAML account or has an inactive SCIM identity` while inviting members to your group

## Description

When attempting to invite a member to your group, you receive a `The member's email address is not linked to a SAML account or has an inactive SCIM identity` error message

## Environment

- Occurs when adding users to a group that has SAML SSO required for group access

- **Impacted offerings:**
  - GitLab.com
  - GitLab Dedicated

- **Impacted versions:**
  - All

## Workarounds

Workaround 1:

1. Have the end user [link SAML to their existing GitLab.com account](https://docs.gitlab.com/ee/user/group/saml_sso/#link-saml-to-your-existing-gitlabcom-account).
1. After the user has done this, initiate a SCIM sync from your identity provider. If the SCIM sync completes without the same error, GitLab has successfully linked the SCIM identity to the existing user account, and the user should now be able to sign in using SAML SSO.

Workaround 2:
If the error persists, the user most likely already exists with both a SAML and SCIM identity, and the SCIM identity that is set to `active: false`. To resolve this:

1. Locate your SCIM token.
1. Use the API to get a [single SCIM provisioned user](https://docs.gitlab.com/ee/development/internal_api/index.html#get-a-single-scim-provisioned-user). Example:

    ```bash
    curl "https://gitlab.example.com/api/scim/v2/groups/test_group/Users/f0b1d561c-21ff-4092-beab-8154b17f82f2" \
     --header "Authorization: Bearer <your_scim_token>" --header "Content-Type: application/scim+json"
    ```

1. Check the returned information to make sure that:
    - The user’s identifier (id) and email match what your identity provider is sending.
    - `active` is set to `false`.
1. If the information does not match, [contact GitLab Support](https://about.gitlab.com/support/#contact-support).
1. If the information matches, use the API to update the SCIM provisioned user’s active value to true. Example:

    ```bash
    curl --verbose --request PATCH "https://gitlab.example.com/api/scim/v2/groups/test_group/Users/f0b1d561c-21ff-4092-beab-8154b17f82f2" \
     --data '{ "Operations": [{"op":"replace","path":"active","value":"true"}] }' \
     --header "Authorization: Bearer <your_scim_token>" --header "Content-Type: application/scim+json"
    ```
    
    - If the update returns a status code 204, have the user attempt to sign in using SAML SSO.

## Cause

This error occurs when a user being invited to your group does not match an active SAML account or SCIM identity.

## Related links

- [Member’s email address is not linked error in SCIM log](https://docs.gitlab.com/ee/user/group/saml_sso/troubleshooting_scim.html#members-email-address-is-not-linked-error-in-scim-log)
- [Generate a SCIM token](https://docs.gitlab.com/ee/user/group/saml_sso/scim_setup.html#configure-gitlab)