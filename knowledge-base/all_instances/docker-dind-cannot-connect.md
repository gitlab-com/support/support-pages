# Encountering `Cannot connect to the Docker daemon at tcp://docker:2375` in CI/CD jobs when using Docker-in-Docker

## Issue

When using Docker-in-Docker v19.03 in GitLab CI/CD jobs, users encounter an error message:

- `Cannot connect to the Docker daemon at tcp://docker:2375`

## Environment

- GitLab Runner using Docker executor or Kubernetes executor.
- Docker-in-Docker (`dind`) v19.03 or later specified in CI/CD jobs.

**Impacted offerings:**
  - GitLab.com
  - GitLab Dedicated
  - GitLab Self-Managed

## Cause

Docker is unable to connect to the Docker daemon over TLS.

Docker 19.03 and later uses TLS by default to connect to the Docker daemon. GitLab Runner must be configured to use TLS, or TLS must be explicitly turned off.

## Resolution

Configure GitLab Runner to use TLS to connect to the Docker daemon:

- [Docker-in-Docker with TLS enabled in the Docker executor](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#docker-in-docker-with-tls-enabled-in-the-docker-executor)
- [Docker-in-Docker with TLS enabled in Kubernetes](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#docker-in-docker-with-tls-enabled-in-kubernetes)

If TLS must be disabled:

- [Docker-in-Docker with TLS disabled in the Docker executor](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#docker-in-docker-with-tls-disabled-in-the-docker-executor)
- [Docker-in-Docker with TLS disabled in Kubernetes](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#docker-in-docker-with-tls-disabled-in-kubernetes)

## Additional information

- The following error message can be found in the GitLab CI/CD job log:
  ```shell
  docker: Cannot connect to the Docker daemon at tcp://docker:2375. Is the docker daemon running?
  ```
- You should always specify a version tag when using a Docker image (e.g. `docker:24.0.5` instead of `docker:latest`) to avoid incompatibility problems with new version releases.
- This error can also occur with the Kubernetes executor when attempts are made to access the Docker-in-Docker service before it has fully started up. See [this issue](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27215) for more details.
- Seeing a combination of ports `2375` and `2376` reported in the CI/CD logfile errors indicates a mismatch between the TLS config in the `.gitlab-ci.yml` and the Runners `config.toml`
- The `DOCKER_HOST: tcp://<hostname>:2375` set within the `.gitlab-ci.yaml` only has 2 accepted values:
   1. `docker` for Docker 19.03 and above
   1. `localhost` for all versions prior to Docker 19.03

## Related links

- [Known issues with Docker-in-Docker](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#known-issues-with-docker-in-docker)
- Blog post: [Update: Changes to GitLab CI/CD and Docker in Docker with Docker 19.03](https://about.gitlab.com/blog/2019/07/31/docker-in-docker-with-docker-19-dot-03/)
- Feature request issue: [Create readiness probes for services in kubernetes executor](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27215)