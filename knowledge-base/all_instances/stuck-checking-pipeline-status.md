# Stuck on "Checking pipeline status"

## Description

The pipeline status in a merge request may remain stuck with a spinning status icon indicating the message "Checking pipeline status".
This may give an impression that the pipeline will soon run.

Logs would indicate the error:
```
The resulting pipeline would have been empty. Review the rules configuration for the relevant jobs.
```

## Environment

Projects' merge requests that:
- Use `rules`
- Have enabled [Pipelines must suceed](https://docs.gitlab.com/ee/user/project/merge_requests/auto_merge.html#require-a-successful-pipeline-for-merge).

- **Impacted offerings:**
  
  - GitLab.com
  - GitLab Dedicated
  - GitLab Self-Managed

## Solution

Check your CI/CD configuration for any [rules](https://docs.gitlab.com/ee/ci/yaml/index.html#rules) or [`workflow:rules`](https://docs.gitlab.com/ee/ci/yaml/index.html#workflowrules) that should trigger a pipeline and if they are met.

From the example below, the spinning status icon is seen if the commit branch is not `staging`:

```
rules:
    - if: $CI_COMMIT_BRANCH == 'staging' && $CI_COMMIT_REF_PROTECTED == 'true'
```

## Cause

This may be caused when there are no `rules` in your CI/CD configuration that match to trigger a pipeline execution. The pipeline is therefore skipped.

## Additional Information

See other possible causes in the [documentation](https://docs.gitlab.com/ee/ci/debugging.html#checking-pipeline-status-message).

## Related Links

- [Proposal: Improve "Checking pipeline status." on merge request interface.](https://gitlab.com/gitlab-org/gitlab/-/issues/499839)
- [Job in child pipeline is not created when the pipeline runs](https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines_troubleshooting.html#job-in-child-pipeline-is-not-created-when-the-pipeline-runs)