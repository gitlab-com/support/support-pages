<!-- This template is meant to be used for specific issues with specific resolution steps. -->

# Encountering `.netrc parse error` while running CI/CD pipeline
<!-- Use sentence-case capitalization for the title. -->

## Issue

CI/CD pipelines using `.netrc` for authentication credentials can encounter the following errors if they use `libcurl 8.11.0`.

```
fatal: unable to access 'https://<URL>/<PROJECT>/': .netrc parser error
```

## Environment

- **Impacted offerings:**
  - GitLab.com
  - GitLab Self-Managed

- **Impacted versions:**
  - libcurl 8.11.0

## Cause

This is related to this curl bug:  [git fetch fails .netrc parse with libcurl 8.11.0](https://github.com/curl/curl/issues/15496)

To identify what version of libcurl you are running you can use the command:

```
# curl --version
curl 8.11.0 (x86_64-alpine-linux-musl) libcurl/8.11.0 OpenSSL/3.3.2 zlib/1.3.1 brotli/1.1.0 zstd/1.5.6 c-ares/1.33.1 libidn2/2.3.7 libpsl/0.21.5 nghttp2/1.62.1
Release-Date: 2024-11-06
Protocols: dict file ftp ftps gopher gophers http https imap imaps ipfs ipns mqtt pop3 pop3s rtsp smb smbs smtp smtps telnet tftp ws wss
Features: alt-svc AsynchDNS brotli HSTS HTTP2 HTTPS-proxy IDN IPv6 Largefile libz NTLM PSL SSL threadsafe TLS-SRP UnixSockets zstd
```


## Workaround

1. Pin to an older version of curl, libcurl, or git in your pipeline. For example: 

```
apk add --no-cache "curl<=8.10.1"
```

2. If using docker images to run your pipeline with the updated libcurl dependency, try pinning it to an older version. For example:

```
image: golang:1.23-alpine3.19
```