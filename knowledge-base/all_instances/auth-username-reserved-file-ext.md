# Encountering "Username ending with reserved file extension not allowed" with LDAP or SAML SSO sign in

## Issue

New users encounter an error message when attempting to log in with LDAP or SAML SSO:

- `Username Ending with Reserved File Extension Not Allowed`

## Environment

- Users provisioned through some [third-party authentication providers](https://docs.gitlab.com/ee/administration/auth/) including:
  - LDAP
  - SAML SSO

- **Impacted offerings:** 
  - GitLab Dedicated
  - GitLab Self-Managed

- **Impacted versions:**
  - 13.12.10 to 13.12.15
  - 14.0.6 to current

## Cause

This happens in environments where a new GitLab user created from LDAP or SAML SSO configuration would include a `.` (e.g. `jasmin.ico`).
 
In 13.12.10 and 14.0.6, GitLab [introduced validation](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/65954)
disallowing usernames that mimic a file name and extension. This avoids a problem where a user's
profile page [could be made inaccessible](https://about.gitlab.com/releases/2021/07/01/security-release-gitlab-14-0-2-released/#denial-of-service-of-user-profile-page).

## Workaround

[Manually create a user](https://docs.gitlab.com/ee/user/profile/account/create_accounts.html) that:

- Has the same primary email address as the LDAP or SAML SSO identity.
- Uses a different username that does not look like a filename (e.g. `jasmin-ico` instead of `jasmin.ico`).

When the user attempts to log in with LDAP or SAML SSO, they will be automatically matched to the
manually created account.
 
## Additional Information

The error message `Username ending with reserved file extension not allowed.` can be found in the `application_json.log`.

## Related Links

- Feature request issue: [GitLab should allow usernames which match file extensions](https://gitlab.com/gitlab-org/gitlab/-/issues/350273)
- Security issue: [A profile page of a user can be denied from loading by appending .html to the username](https://gitlab.com/gitlab-org/gitlab/-/issues/26295)
