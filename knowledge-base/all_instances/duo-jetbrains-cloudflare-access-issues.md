# GitLab Duo Jetbrains fails with SSL error to instance behind Cloudflare Access

## Description

When setting up GitLab Duo in JetBrains IDEs, you may received the following error:

```
Failed to check token: FetchError: request to https://gitlab.example.com/api/v4/personal_access_tokens/self failed, reasion: self-signed certificate in certificate chain
```

## Environment

This applies to GitLab environments behind Cloudflare Access.

- **Impacted offerings:**
  - GitLab.com
  - GitLab Dedicated
  - GitLab Self-Managed

## Solution

Follow the [instructions in our documentation](https://docs.gitlab.com/ee/editor_extensions/jetbrains_ide/jetbrains_troubleshooting.html#certificate-errors) to specify a CA certificate or **Pass CA certificate from Duo to the Language Server**.

## Cause

Cloudflare Access is a reverse proxy that works in front of GitLab. Since the Language Server does not detect certificates on your local system, it needs to be informed of the certificate used by Cloudflare Access.

## Related Links

Review the [troubleshooting documentation](https://docs.gitlab.com/ee/editor_extensions/jetbrains_ide/jetbrains_troubleshooting.html) for JetBrains IDEs.
