# GitLab Runner 17.5.0 fails to start

## Issue

GitLab Runner 17.5.0 fails to start, with the following error messages appearing in the logs:

- `"FATAL: failed to get user home dir: $HOME is not defined"` 
- `Remove-Item: Cannot process command because of one or more missing mandatory parameters: Path.`

## Environment

- **Impacted offerings:**
  - GitLab.com
  - GitLab Dedicated
  - GitLab Self-Managed

- **Impacted versions:**
  - GitLab Runner v17.5.0

## Cause

In GitLab Runner 17.5.0, we changed `HOME` directory detection and fixing to avoid the use of a
deprecated function from the `homedir` Go library. The new method did not work correctly in
configurations that were previously working.

## Resolution

- Upgrade to GitLab Runner 17.5.1.

## Additional information

- The error messages can be viewed in the [GitLab Runner service logs](https://docs.gitlab.com/runner/faq/#view-the-logs).

## Related links

- Bug fix merge request: [Fix home directory detection](https://gitlab.com/gitlab-org/gitlab-runner/-/merge_requests/5087)
- Bug report: [Runner 17.5 on Local RHEL 9 hosts are failing with error: "FATAL: failed to get user home dir: $HOME is not defined"](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/38252)
- Bug report: [Windows Runner 17.5.0 broken](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/38254)
- Bug report: [systemd files missing on Ubuntu in 17.5.0](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/38255)
- Bug report: [Runner 17.5 breaks cross project submodule authentication with job token](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/38256)
