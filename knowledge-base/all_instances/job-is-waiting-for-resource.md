# Resolve `job is waiting for resource` problem

## Description

Jobs can be stuck in `Canceling` state, despite having been cancelled.
This means that the resource they use is never freed up,
and subsequent jobs thus indefinitely show the message:

```plaintext
This job is waiting for resource: <your-resource>
```

## Environment

CI pipelines that use [Resource](https://docs.gitlab.com/ee/ci/yaml/#resource_group)
[groups](https://docs.gitlab.com/ee/ci/resource_groups/).

- **Impacted offerings:**
  - GitLab Self-Managed
  - GitLab Runner

## Solution

A workaround is provided in [gitlab#467107](https://gitlab.com/gitlab-org/gitlab/-/issues/467107#note_2097032321).

1. Update that Rails snippet with the affected _pipeline_ (not job) IDs.
1. Launch [the Rails console](https://docs.gitlab.com/ee/administration/operations/rails_console.html).
3. Run your updated Rails snippet.

## Cause

The job's state machine does not properly transition it from `canceling` to `canceled`.
Further details are being investigated, see [gitlab#467107](https://gitlab.com/gitlab-org/gitlab/-/issues/467107).

## Additional Information

GitLab Dedicated and GitLab.com can also be impacted,
but a support ticket will be required to request one of our admins to perform the workaround.

## Related Links
