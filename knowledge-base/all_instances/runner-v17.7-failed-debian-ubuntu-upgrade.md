# GitLab Runner 17.7.0: Failed upgrade via apt on Debian/Ubuntu

## Issue

When upgrading to GitLab Runner 17.7.0 from an older version, `apt` may output this error:

```
Preparing to unpack .../gitlab-runner-helper-images_17.7.0-1_all.deb ...
Unpacking gitlab-runner-helper-images (17.7.0-1) ...
dpkg: error processing archive /var/cache/apt/archives/gitlab-runner-helper-images_17.7.0-1_all.deb (--unpack):
 trying to overwrite '/usr/lib/gitlab-runner/helper-images/prebuilt-ubuntu-x86_64-pwsh.tar.xz', which is also in package gitlab-runner 17.6.0-1
dpkg-deb: error: paste subprocess was killed by signal (Broken pipe)
Preparing to unpack .../gitlab-runner_17.7.0-1_amd64.deb ...
Unpacking gitlab-runner (17.7.0-1) over (17.6.0-1) ...
Errors were encountered while processing:
 /var/cache/apt/archives/gitlab-runner-helper-images_17.7.0-1_all.deb
```

## Environment

- **Impacted offerings:**
  - GitLab Runner

- **Impacted versions:**
  - 17.7.0

## Cause

The [GitLab Runner helper images](https://docs.gitlab.com/runner/configuration/advanced-configuration.html#helper-image) were moved to a separate package as part of GitLab Runner 17.7.0, but was done incorrectly.

## Workaround

Explicitly uninstall Gitlab Runner, then install it again by running the commands:

```
# This will remove the gitlab-runner package, but leave configuration files on the system.
apt remove gitlab-runner

apt install gitlab-runner
```

## Related links

- Issue: [Broken debian packaging after MR !5190](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/38394).
- [GitLab Runner MR 5190](https://gitlab.com/gitlab-org/gitlab-runner/-/merge_requests/5190)
