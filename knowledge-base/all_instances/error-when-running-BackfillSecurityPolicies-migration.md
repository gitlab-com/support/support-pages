<!-- This template is meant to be used for specific issues with specific resolution steps. -->

# Failed background migration - "BackfillSecurityPolicies: security_orchestration_policy_configurations"
<!-- Use sentence-case capitalization for the title. -->

## Issue

After upgrade to 17.6.x, the background migration fails with the error:

```plaintext
PG::CheckViolation: ERROR: new row for relation "security_policies" violates check constraint "check_99c8e08928"
```

## Environment

- **Impacted offerings:**
  - GitLab Dedicated
  - GitLab Self-Managed

- **Impacted versions:**
  - 17.6.x

## Cause

The migration fails when the `description` field of a row in the `security policy` table is longer than 255 characters.

## Resolution

The constraint for the `description` field was removed in GitLab 17.7 [MR 172933](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/172933). The migration will self-fix after upgrading to 17.7 or later.

This migration does not impact GitLab functionality and may be left in a failed state if immediate upgrade is not possible.

## Additional information

Public discussion of the error with the development team can be found in [this merge request thread](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/172933#note_2250839100).

## Related links

- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/172933
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/172988
