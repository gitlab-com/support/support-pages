# GitLab Runner 17.5: Minio cache uploads not working

## Issue

On GitLab Runner 17.5, Minio cache uploads no longer work, causing CI/CD job cache to be stored
locally. The following message is seen in the job log:

- `No URL provided, cache will not be uploaded to shared cache server. Cache will be stored only locally.`.

## Environment

- Minio configured as object storage for [distributed runners caching](https://docs.gitlab.com/runner/configuration/autoscale.html#distributed-runners-caching) without `BucketLocation` defined.

- **Impacted offerings:**
  - GitLab.com
  - GitLab Dedicated
  - GitLab Self-Managed

- **Impacted versions:**
  - GitLab Runner v17.5.0 to v17.5.1

## Cause

In GitLab Runner 17.5, we [switched to using the AWS SDK for S3 cache access](https://gitlab.com/gitlab-org/gitlab-runner/-/merge_requests/4987).
The AWS SDK does not have a default `BucketLocation`, causing some configurations to fail.

## Resolution

- Upgrade to GitLab Runner 17.5.2.

## Additional information

- In GitLab Runner 17.5.2, we default `BucketLocation` to `us-east-1` to maintain backwards
  compatibility.

## Related links

- Issue: [S3 cache with MinIO is not working anymore since update to gitlab-runner 17.5.1](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/38264).
- Fix: [Default to us-east-1 region for AWS SDK v2](https://gitlab.com/gitlab-org/gitlab-runner/-/merge_requests/5093)
