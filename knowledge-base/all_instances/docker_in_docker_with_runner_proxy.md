# Docker in docker (DinD) with runner behind a proxy cannot resolve DNS

## Description

Output from `docker info` shows:

```
Server:
ERROR: Error response from daemon: <HTML><HEAD>
<TITLE>Network Error</TITLE>
...
<big>Network Error (dns_unresolved_hostname)</big>
...
Your requested host "docker" could not be resolved by DNS.
```

## Environment

GitLab Runner behind a proxy

- **Impacted offerings:**
  - GitLab.com
  - GitLab Dedicated
  - GitLab Self-Managed

## Solution

Follow the documentation on [Proxy settings when using dind service](https://docs.gitlab.com/runner/configuration/proxy.html#proxy-settings-when-using-dind-service). The documentation describes two approaches for configuring proxy settings that will allow your docker client to connect to dockerd on the service container:

- Add proxy configuration to your GitLab runner configuration
- Add proxy configuration steps in your build job

## Cause

The Docker client in your CI job needs to communicate with the Docker daemon running in the service container. When behind a proxy, this communication fails unless proper proxy settings are configured for both the runner and the Docker service.

## Related Links

- build an image [using DinD method](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-in-docker) 
