# Zoekt indexer fails with "Error while dialing: dial unix ///var/opt/gitlab/gitaly/gitaly.socket"

## Description

- Zoekt indexer fails to start or operate correctly. Logs show the error:

```
"rpc error: code = Unavailable desc = last connection error: connection error: desc = \"transport: Error while dialing: dial unix ///var/opt/gitlab/gitaly/gitaly.socket: connect: no such file or directory\""
```
- Zoekt is unable to connect to Gitaly, preventing proper indexing of repositories.

## Environment


- GitLab with exact code search running on single node install
- Zoekt running on a different server

- **Impacted offerings:**
  - GitLab Self-Managed

## Solution

1. Configure Gitaly to listen on a network port instead of a Unix socket.
2. Update the GitLab Rails configuration to use the network address for Gitaly.
3. Apply the changes by running: `gitlab-ctl reconfigure`

Here is an example configuration for GitLab 17.7 and earlier:

```ruby
gitlab_rails['gitaly_token'] = 'abc123secret'

git_data_dirs({
  'default'  => { 'gitaly_address' => 'tcp://gitlab.example.com:8075' }
})

gitaly['configuration'] = {
   listen_addr: '0.0.0.0:8075',
   auth: {
     token: 'abc123secret',
   },
   storage: [
      {
         name: 'default',
         path: '/var/opt/gitlab/git-data/repositories',
      },
    ]
}
```

Here is an example configuration for GitLab 17.8 and later:

```ruby
gitlab_rails['gitaly_token'] = 'abc123secret'

gitlab_rails['repositories_storages'] = {
  'default'  => { 'gitaly_address' => 'tcp://gitlab.example.com:8075' },
}

gitaly['configuration'] = {
   listen_addr: '0.0.0.0:8075',
   auth: {
     token: 'abc123secret',
   },
   storage: [
      {
         name: 'default',
         path: '/var/opt/gitlab/git-data/repositories',
      },
    ]
}
```

We can verify that Zoekt indexer is now working if we see the following logs from the Zoekt indexer process when we create a new project on a namespace where Zoekt is configured:

```
time=2025-02-17T05:24:59.744Z level=DEBUG msg=TaskRequest status=200 body="{\"id\":1,\"truncate\":false,\"tasks\":[{\"name\":\"index\",\"payload\":{\"GitalyConnectionInfo\":{\"Address\":\"tcp://gitlab:8075\",\"Token\":\"abc123secret\",\"Storage\":\"default\",\"Path\":\"@hashed/4a/44/4a44dc15364204a80fe80e9039455cc1608281820fe2b24f1e5233ade6af1dd5.git\"},\"Callback\":{\"name\":\"index\",\"payload\":{\"task_id\":81}},\"RepoId\":10,\"FileSizeLimit\":1048576,\"Timeout\":\"5400s\"}}],\"optimized_performance\":false,\"pull_frequency\":\"10s\"}"
```

## Cause

This issue occurs because Zoekt needs to connect directly to Gitaly. In a single-node Omnibus installation, GitLab Rails is configured to use a Unix socket for Gitaly communication. However, when Zoekt is running on a different server, it cannot access this socket, leading to the connection error.

## Related Links

- [Configure Gitaly](https://docs.gitlab.com/administration/gitaly/configure_gitaly/)
- [Docs: Include examples for running gitaly on one node omnibus with port exposed ](https://gitlab.com/gitlab-org/gitlab/-/issues/517498)