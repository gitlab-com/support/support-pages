# Git operations for projects in namespaces with trailing hyphens are broken

## Description 

- Git operations are forbidden with 403 error for projects found under legacy groups with trailing hyphens in their namespace
- Example of a project namespace with trailing hyphen: `https://gitlab.com/this-is-a-group/group-with-trailing-hyphen-/this-is-a-project` 
- Git operations performed by runners in CI jobs are also impacted, resulting in jobs failure
- Users will observe the error message `remote: Nil JSON web token fatal: unable to access '<PROJECT_URL>.git/': The requested URL returned error: 403`

## Environment

- **Impacted offerings:**
  - GitLab Self-Managed

- **Impacted versions:**
  - 17.8.2
  - 17.7.4
  - 17.6.5 

## Solution 

Upgrade to an unaffected version of GitLab.

- Valid versions for 17.8.2
  - 17.9 or later
- 17.7.4
  - 17.9 or later
  - 17.8.0 - 17.8.1
- 17.6.5
  - 17.9 or later
  - 17.8.0 - 17.8.1
  - 17.7.0 - 17.7.3