# 503 Error when accessing GitLab Dedicated instance

## Description

- Navigating to your GitLab Dedicated instance returns `503 Service Temporarily Unavailable`.
- You may see this when viewing the web UI, within API responses, or via Git.
- This typically occurs during scheduled maintenance windows, which are performed weekly during region-specific times.

## Environment

- **Impacted offerings:**
  - GitLab Dedicated

## Solution

1. Check if your instance is currently in a maintenance window:
   - Review the schedule in the [GitLab Dedicated maintenance windows](https://docs.gitlab.com/ee/administration/dedicated/maintenance.html#maintenance-windows).
   - Sign in to [Switchboard](https://console.gitlab-dedicated.com/) to check your instance status.
2. If your GitLab Dedicated instance is currently under maintenance, you will need to wait until the maintenance window has completed.
3. If you continue to see the `503 Service Temporarily Unavailable` error outside of a maintenance window, [contact GitLab Support](https://about.gitlab.com/support/#contact-support).

## Cause

This error generally occurs when your GitLab Dedicated instance is undergoing scheduled maintenance.

## Related Links

- [GitLab Dedicated maintenance windows](https://docs.gitlab.com/ee/administration/dedicated/maintenance.html#maintenance-windows)
