# GitLab Duo bot user taking a license seat

## Description

In GitLab version 17.7 and 17.8, GitLab Duo bot user has the `is using seat` label in the GitLab UI. The GitLab Duo bot is not actually taking a license seat.

This is a known UI bug and it's fixed in this [MR](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/177269) for GitLab 17.9.

## Environment

- **Impacted offerings:**
  - GitLab Self-Managed
  - GitLab Dedicated

- **Impacted versions:**
  - 17.7 to 17.8

## Cause

Bug issue: [UI shows GitLabDuo bot is taking up a license seat](https://gitlab.com/gitlab-org/gitlab/-/issues/512262)