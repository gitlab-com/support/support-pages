# Support - Jira Troubleshooting

1. Clarify with customers the following:
    - Which integration they are referring to? We have GitLab Jira Integration and Jira Development Panel integration which uses GitLab for Jira Cloud app.
    - Which Jira version - Cloud or Server? If Server which version? (Note: 8.14 later [links differently](https://confluence.atlassian.com/adminjiraserver/linking-gitlab-accounts-1027142272.html))
1. Check their configuration, walk through the steps.
   **NOTE**:
   Some customers confused the configuration of the two integrations, for example using the `jira` user created in GitLab Jira Integration as the Term/Account for setting up DVCS account for their Jira Development Panel. This will load the repo of the `jira` user's personal namespace which usually has 0 repos.
1. If all the configurations are correct, review the troubleshooting pages for the specific integration:
   - [Jira issue integration](https://docs.gitlab.com/ee/integration/jira/troubleshooting.html)
   - [GitLab for Jira Cloud app](https://docs.gitlab.com/ee/administration/settings/jira_cloud_app_troubleshooting.html)
   - [Jira DVCS connector](https://docs.gitlab.com/ee/integration/jira/dvcs/troubleshooting.html)
   - [integrations_json.log](https://docs.gitlab.com/ee/administration/logs/#integrations_jsonlog), [Sidekiq](https://docs.gitlab.com/ee/administration/logs/#sidekiq-logs), [production_json.log](https://docs.gitlab.com/ee/administration/logs/#production_jsonlog), browser developer tools

## Jira issue integration

### Get any Jira issue integration logs

```
jq '. | select(.["meta.caller_id"] == "Integrations::CreateExternalCrossReferenceWorker")' /var/log/gitlab/gitlab-rails/integrations_json.log
```

### Confirm the credentials are correct and the issue exists

```
curl --verbose --user "$USER:$API_TOKEN" "https://$ATLASSIAN_SUBDOMAIN.atlassian.net/rest/api/2/issue/$JIRA_ISSUE"
```

### Rails console: Confirm we can post a comment to a Jira issue

```
project = Project.find_by_full_path(“group/project”)
integration = project.integrations.find_by(type: “Integrations::Jira”)
jira_issue = integration.client.Issue.find(<JIRA_ISSUE_ID>)
jira_issue.comments.build.save!(body: 'This is a test comment')
```


## GitLab for Jira Cloud app

### Get any logs related to setting up the GitLab for Jira Cloud app

```
rg '/-/(manifest.json|jira_connect)|/oauth/|/api/v4/(groups|users|integrations/jira_connect)' /var/log/gitlab
```

### Get any development panel logs once configured

```
jq 'select(.["meta.caller_id"] | test("^JiraConnect"))' /var/log/gitlab/gitlab-rails/integrations_json.log
jq 'select(.["meta.caller_id"] | test("^JiraConnect"))' /var/log/gitlab/sidekiq/current
```

### Searching Kibana for Jira Connect proxy issues

Refer to the [official documentation](https://docs.gitlab.com/ee/administration/settings/jira_cloud_app_troubleshooting.html#debugging-jira-connect-proxy-issues)

