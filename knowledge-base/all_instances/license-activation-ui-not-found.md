# Unable to find UI to activate license using a 24-character code

## Description

For GitLab instances on versions between 14.1 and 15.7, it is not possible to activate using a second activation code via the UI.
Customers would only see the `Remove license`, `Manage`, and `Sync subscription details` buttons, depending on their GitLab version.

## Environment

- **Impacted offerings:**
  - GitLab Self-Managed

- **Impacted versions:**
  - 14.1 to 15.7

## Solution OR Workaround

Use GraphQL to activate the license. The response of the GraphQL mutation will also show any errors encountered during the activation. These steps can be used in GitLab versions 14.1 and later.

1. Navigate to `https://<self-managed-gitlab-site.com>/-/graphql-explorer`
1. Run the following mutation by replacing `<activation code>` with the actual 24-character cloud activation code:

```
mutation {
  gitlabSubscriptionActivate(
    input: {
      activationCode: "<activation code>"
    }
  ) {
    errors
    license {
      id
      activatedAt
      startsAt
      usersInLicenseCount
      lastSync
    }
  }
}
```

## Cause

The ability to activate using multiple activation codes was added in GitLab version 15.7,
where a button with the text `Add activation code` is visible in the `Admin` -> `Subscription` page.


## Related links

- Issue: [Ability to enter additional activation code(s) for future subscription(s)](https://gitlab.com/gitlab-org/gitlab/-/issues/374938)
- Support documentation: [GraphQL activation steps](https://handbook.gitlab.com/handbook/support/license-and-renewals/workflows/self-managed/troubleshoot_cloud_licensing/#activating-a-license-with-the-graphql-api)