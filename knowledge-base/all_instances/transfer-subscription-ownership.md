# How to transfer subscription ownership

This article describes the steps you can take to transfer ownership of a GitLab subscription in different scenarios. Follow the guide that best aligns with your situation.

## Definitions

- **Billing account manager**: Has access to view and manage subscriptions and billing account information in the Customers Portal.
- **Subscription contact**: The subscription owner and the primary contact for your billing account. Receives subscription event notifications and information about applying the subscription. Must be a billing account manager. Also known as the "Sold to" contact.
- **Billing contact**: Receives all invoices and subscription event notifications. They do not have a Customers Portal account with access to the subscription unless they are also a billing account manager. Also known as the "Bill to" contact.

## Ownership transfer by existing subscription contact

If you are the current subscription contact and want to transfer ownership to a different person:

1. [Change your profile information](https://docs.gitlab.com/ee/subscriptions/customers_portal.html#change-profile-owner-information) to the new contact's details and email address.
1. Have the new contact sign into the Customers Portal with their email address using a [one-time sign-in link](https://customers.gitlab.com/customers/sign_in?legacy=true).
1. Have the new contact [change the linked GitLab.com account](https://docs.gitlab.com/ee/subscriptions/customers_portal.html#change-the-linked-account) to their own GitLab.com account.

## Subscription contact has left your organization

If you have access to the subscription contact's email mailbox:

1. Sign into the Customers Portal with the subscription contact's email address using a [email one-time sign-in link](https://customers.gitlab.com/customers/sign_in?legacy=true).
1. [Change the profile information](https://docs.gitlab.com/ee/subscriptions/customers_portal.html#change-profile-owner-information) to your details.
1. [Change the linked GitLab.com account](https://docs.gitlab.com/ee/subscriptions/customers_portal.html#change-the-linked-account) to your GitLab.com account.

If you do not have access to the subscription contact's email mailbox:

Contact Support to request that the subscription ownership be changed using the [Subscriptions, License or Customers Portal Problems form](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293).

You must provide the [proof of ownership](https://handbook.gitlab.com/handbook/support/license-and-renewals/workflows/customersdot/associating_purchases/#ownership-verification) for Support to proceed with your request.

You can use the following template when opening [the Support ticket](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293):

> Hi Support,
> 
> Please update subscription ownership for my subscription/billing account. I confirm that I am not able to make this change via the Customers Portal. Here are the relevant details:
> 
> - Old subscription contact email address:
> - New subscription contact email address: 
> - (Optional) Subscription or Billing account name: 
> - Verification proof: 

## Transfer ownership and retain access to Customers Portal

The billing account manager can [update the subscription contact](https://docs.gitlab.com/ee/subscriptions/customers_portal.html#change-your-subscription-contact) to any listed billing account manager in the Customers Portal.

To transfer ownership to a contact who is not listed as a billing account manager:

1. Ensure that the contact has a Customers Portal account.
1. Contact Support using the [Subscriptions, License or Customers Portal Problems form](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293) to request that the contact be added as an additional billing account manager.

## Troubleshooting

### Error: Email has already been taken

The Customers Portal will not allow you to update your profile to use an email address already in use by another account. You can provide an alternative email address, or contact Support using the [Subscriptions, License or Customers Portal Problems form](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293) to transfer the subscription contact ownership on your behalf.

## Related links

- [Change profile owner information](https://docs.gitlab.com/ee/subscriptions/customers_portal.html#change-profile-owner-information)
- [Change subscription contact](https://docs.gitlab.com/ee/subscriptions/customers_portal.html#change-your-subscription-contact)
- [Change billing contact](https://docs.gitlab.com/ee/subscriptions/customers_portal.html#change-your-billing-contact)
- [Troubleshoot billing account manager updates](https://docs.gitlab.com/ee/subscriptions/customers_portal.html#troubleshooting-your-billing-or-subscription-contacts-name)