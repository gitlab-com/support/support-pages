<!--
Use when:
- Documenting solutions for specific errors or problems
- Addressing system failures or malfunctions
- Providing troubleshooting steps for known issues
- Explaining how to recover from common failures

For a more detailed style guide see https://handbook.gitlab.com/handbook/support/knowledge-base/kb-style-guide/ 
-->

# Error on save of Duo Local AI Gateway URL - 'An error occurred while updating your settings.'

## Description

`An error occurred while updating your settings. Reload the page to try again.` error occurs
  when trying to save Local AI Gateway URL to one of the following:
  
- `localhost`
- `127.0.0.1`
- The internal IP address of the GitLab instance.
- A hostname which resolves to one of the above URLs.

## Environment

- Duo self-hosted models configured

**Impacted offerings:**

- GitLab Self-Managed
 
**Impacted versions**

- 17.9.0 and later

## Solution

Use a non-local hostname or IP address in the Local AI Gateway URL as [documented](https://docs.gitlab.com/install/install_ai_gateway/#install-using-docker).

## Cause

The `Local AI Gateway URL` has [validation](https://gitlab.com/gitlab-org/gitlab/-/blob/v17.9.0-ee/ee/app/models/ai/setting.rb?ref_type=tags#L42)
applied where access to the GitLab instance via a local address is denied by default.
