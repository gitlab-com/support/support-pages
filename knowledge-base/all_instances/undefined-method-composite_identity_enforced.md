# 500 `undefined method composite_identity_enforced` error during log in

## Description

- After upgrading to 17.7.x, users receive a `500` error at log in with exception:
  `"exception.class":"NoMethodError","exception.message":"undefined method composite_identity_enforced'`

## Environment

**Impacted offerings:**
- GitLab Self-Managed

**Impacted versions:**
- 17.7.x

## Solution

Run the migration again with `gitlab-rake db:migrate:up VERSION=20241127135043`

## Cause

The `20241127135043 AddCompositeIdentityEnforcedToUsers` migration didn't complete accurately.

## Additional Information

Check if the migration is `up` or `down` with `gitlab-rake db:migrate:status | grep AddCompositeIdentityEnforcedToUsers`.
