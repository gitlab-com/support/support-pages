# GitLab reconfigure/upgrade error - `Reading unsupported config value grafana`

## Description

GitLab reconfigure/upgrade fails with errors:

```
There was an error running gitlab-ctl reconfigure:

Reading unsupported config value grafana.
```

```
FATAL: Mixlib::Config::UnknownConfigOptionError: Reading unsupported config value grafana.
```

## Environment

**Impacted offerings:**
  - GitLab Self-Managed

**Impacted versions:**
  - 16.3 and later

## Solution

- Remove all references to `grafana` in your configuration
  - e.g. `grafana["enable"]`

## Cause

- Bundled Grafana was removed in `16.3`

## Related Links

- [Bundled Grafana deprecated and disabled](https://docs.gitlab.com/ee/update/deprecations.html#bundled-grafana-deprecated-and-disabled)
- [GitLab 16 changes - Linux package installations](https://docs.gitlab.com/ee/update/versions/gitlab_16_changes.html#linux-package-installations-8)
- [Presence of `grafana` keys in `gitlab.rb` prevents upgrade](https://gitlab.com/gitlab-org/omnibus-gitlab/-/issues/8773)