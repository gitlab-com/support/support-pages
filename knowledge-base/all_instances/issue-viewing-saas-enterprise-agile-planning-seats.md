# Issue viewing Enterprise Agile Planning seats

## Description

- Viewing license information on GitLab does not list Agile Planning seats. Instead, the Ultimate license seat count matches the number of Ultimate and Agile Planning licenses combined.
- Enterprise Agile Planning seats can be viewed in the [customers portal](https://customers.gitlab.com/customers/sign_in).

## Environment

- **Impacted offerings:**
  - GitLab.com
  - GitLab Dedicated
  - GitLab Self-Managed

- **Impacted versions:**
  - 17.7 and above

## Workaround

1. Confirm the Enterprise Agile Planning seats have been applied in the [customers portal](https://customers.gitlab.com/customers/sign_in).
1. From the GitLab instance, assign the `Planner` role to the relevant users.

## Cause

GitLab currently does not differentiate between Ultimate seats and Enterprise Agile Planning seats. When you have a subscription with Enterprise Agile Planning seats, the seats will be added to the Ultimate seats in `Seats in subscription` on your group billing page for GitLab.com or `Users in subscription` for Self-Managed and GitLab Dedicated.

The [Planner role](https://about.gitlab.com/blog/2024/11/25/introducing-gitlabs-new-planner-role-for-agile-planning-teams/) was introduced to in [GitLab 17.7](https://about.gitlab.com/releases/2024/12/19/gitlab-17-7-released/) to assign Enterprise Agile Planning seats to users.

## Related links

- [How to Assign Enterprise Agile Planning Seats](https://support.gitlab.com/hc/en-us/articles/18409173383068-How-to-Assign-Enterprise-Agile-Planning-Seats)
- [New Planner role feedback issue](https://gitlab.com/gitlab-org/gitlab/-/issues/503817)
- [New Planner user role](https://gitlab.com/gitlab-org/gitlab/-/issues/482733)
- [Approaching license user count banner does not consider EAP seats](https://gitlab.com/gitlab-org/gitlab/-/issues/504777)