# Workspaces creation error - missing GLIBC_2.3x required by devfile gem

## Description

On older OS versions where `GLIBC` version is older than `2.34`, the following error occurs when creating a new workspace:

```
Workspace create devfile flatten failed: /opt/gitlab/embedded/lib/ruby/gems/3.2.0/gems/dev-file-0.0.28.pre-alpha1-x86_64-linux/bin/devfile: /lib/x86_64-linux-gnu/libc.so.6: version 'GLIBC_2.34' not found
```

## Environment

- **Impacted offerings:**
  - GitLab Self-Managed

- **Impacted versions:**
  - GitLab 17.4.x
  - GitLab 17.5.0 - 17.5.4
  - GitLab 17.6.0 - 17.6.1

- **OS Versions:**
  - CentOS 7, Ubuntu 20.04
  - any other OS versions where GLIBC version < 2.34  

## Workaround

Upgrade to GitLab version 17.5.5+, 17.6.2+ or 17.7+

## Cause

GitLab 17.0 added support for workspace creation in `aarch64-linux` environments by upgrading the `devfile` Gem. The newer Gem relies on a newer version of GLIBC, causing workspace creation to fail on operating systems with a GLIBC version of 2.33 and older. 

## Additional information

<!-- Describe any additional information which may assist troubleshooting. e.g. Which log file does the error show up in? -->

## Related links

- [Merge request adding support for `aarch64-linux`]((https://gitlab.com/gitlab-org/gitlab/-/merge_requests/165888))
- [Issue reporting the regression](https://gitlab.com/gitlab-org/gitlab/-/issues/499476)