# GitLab Runner 17.5 pods never become attachable

## Issue

GitLab CI/CD jobs fail with the following message when run with GitLab Runner 17.5 deployed on
Kubernetes:

- `ERROR: Job failed (system failure): prepare environment: pod failed to become attachable timed out waiting for pod to become attachable`.


## Environment

- GitLab Runner Helm chart 0.70.0 and 0.70.1

- **Impacted offerings:**
  - GitLab.com
  - GitLab Dedicated
  - GitLab Self-Managed

- **Impacted versions:**
  - GitLab Runner v17.5.0 and v17.5.1

## Cause

We are investigating the cause in [Runner 17.5.0: Pods never become attachable](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/38260).

## Workaround

Downgrade to GitLab Runner 17.4 (chart 0.69.0).

## Additional information

- Based on user reports, GitLab Runner 17.5 may work correctly on some Kubernetes configurations but
  not on others.

## Related links

- Issue: [Runner 17.5.0: Pods never become attachable](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/38260)
