<!-- This template is meant to be used for specific issues with specific resolution steps. -->

# High number of 500 errors after zero-downtime upgrade
<!-- Use sentence-case capitalization for the title. -->

## Description

<!-- 
Describe
- Break/fix: The symptoms customers will see when they encounter an issue.
- How-to: The situation when the steps in the article are needed
- Q&A: The question a customer may have 
One bullet point per item. Use a bullet point even if there's only one symptom.
-->

- High volume of 500 errors occurring after zero-downtime upgrade of GitLab between 2 or more minor releases
- Logs show `PG::UndefinedColumn` or `PG::UndefinedTable` `ERROR:...does not exist`

## Environment

<!-- Describe the environment configuration in which the description applies. -->

- **Impacted offerings:**
  - GitLab Self-Managed

- **Impacted versions:**
  - Any zero-downtime upgrade that skips a minor version 

## Solution

[Restart the Rails node](https://docs.gitlab.com/ee/administration/restart_gitlab.html)

## Cause

Zero-downtime upgrades can only be done one minor release at a time. As stated in [documented requirements and considerations](https://docs.gitlab.com/ee/update/zero_downtime.html#requirements-and-considerations), you can only upgrade one minor release at a time. If you skip releases, database modifications may be run in the wrong sequence and leave the database schema in a broken state.

## Additional information

You can trace the errors in the logs using the correlation ID returned in the UI. Current errors will be logged in `/var/log/gitlab/gitlab-rails/application_json.log`.

For example, if a zero-downtime upgrade was performed from `17.3.5 to 17.5.3`, you would encounter an error related to the `application_settings.sign_in_text_html` column which was 
removed in `17.4`: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/161594/diffs.

The error in the log would show:

```
{"severity":"WARN","time":"YYYY-MM-DDTHH:MM:SSZ","correlation_id":"01JEGT5324V0RNYWHB048BHXNG","message":"Cached record for ApplicationSetting couldn't be loaded, falling back to uncached record: PG::UndefinedColumn: ERROR: column application_settings.sign_in_text_html does not exist\nLINE 1: ...ol\", \"application_settings\".\"usage_ping_enabled\", \"applicati...\n                               ^\n"}
```


<!-- Add links to feature request or bug issues here. Consider linking other resources as necessary. -->
