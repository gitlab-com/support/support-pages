# High CPU load for database server if project has a large number of pipelines

## Issue

If the database CPU load is very high, it could be caused by the [auto cancel redundant pipelines setting](https://docs.gitlab.com/ee/ci/pipelines/settings.html#auto-cancel-redundant-pipelines). This issue can affect the Sidekiq queue slowing down background jobs processing, causing side-effects such as Pipelines taking a long time picking up jobs, Merge Requests taking a long time to load and overall instance slowness.

## Environment

- **Impacted offerings:**
  - GitLab Self-Managed

- **Impacted versions:**
  - Up to 17.4.2.

## Cause

When [auto-cancel redundant pipelines](https://docs.gitlab.com/ee/ci/pipelines/settings.html#auto-cancel-redundant-pipelines) is enabled for a project, the `CancelRedundantPipelinesService` [uses a database query](https://gitlab.com/gitlab-org/gitlab/-/blob/master/app/services/ci/pipeline_creation/cancel_redundant_pipelines_service.rb?ref_type=heads#L40-L45) to find all pipelines created before a certain date. This query adds a lot of CPU utilization on the database server when a project has a large number of pipelines. 

## Resolution

To identify if this issue is the causation of the CPU load:

1. Check the [Sidekiq queue and utilization](https://docs.gitlab.com/ee/administration/admin_area.html#background-jobs). Look for the `CancelRedundantPipelinesService` worker — it is usually long-running or will have large amounts jobs in the queue.
2. Run the using the [database console](https://docs.gitlab.com/ee/administration/troubleshooting/postgresql.html#start-a-database-console) to identify the projects with the largest amounts of pipelines: 
   ```sql
   select project_id, count(project_id) as pipeline_count from ci_pipelines group by project_id having count(project_id) > 10000 order by pipeline_count desc;
   ```
3. You can [disable auto-cancel redudant pipelines](https://docs.gitlab.com/ee/ci/pipelines/settings.html#auto-cancel-redundant-pipelines) for the projects with the largest number of pipelines, or disable instance-wide by enabling the feature flag `disable_cancel_redundant_pipelines_service` in the [Rails console](https://docs.gitlab.com/ee/administration/operations/rails_console.html#starting-a-rails-console-session):
   ```ruby
   Feature.enable(:disable_cancel_redundant_pipelines_service)
   ```

To workaround this issue without disabling the auto-cancel redundant pipelines settings on the project:

- Allocate more CPU resources to the database server.
- If Sidekiq is overloaded, you might need to [add more Sidekiq processes](https://gitlab.com/gitlab-org/gitlab/-/administration/sidekiq/extra_sidekiq_processes.md#start-multiple-processes) for the `ci_cancel_redundant_pipelines` queue if your projects have a very large number of pipelines.

## Additional information

- [The query was changed in 17.5](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/164436)

## Related links

- Bug report: [Query used by CancelRedundantPipelinesService can cause high CPU load for database server if project has a large number of pipelines](https://gitlab.com/gitlab-org/gitlab/-/issues/435250)

