# Code owner group is empty and optional in approvals

## Description

- Code owners is empty despite settings in the [CODEOWENRS file](https://docs.gitlab.com/ee/user/project/codeowners/index.html#codeowners-file).
- Merge requests on a protected branch show the Code Owners group as empty and optional.  

## Environment

- **Impacted offerings:**
  - GitLab.com
  - GitLab Dedicated
  - GitLab Self-Managed

## Solution

Ensure users and groups exist and have access to the project.

- Syntax in the CODEOWNERS file for groups and subgroups is case-sensitive and must match the [group path](https://docs.gitlab.com/ee/user/group/manage.html#change-a-groups-path). 
- Verify correct [group inheritence](https://docs.gitlab.com/ee/user/project/repository/branches/protected.html#group-inheritance-and-eligibility) for protected branches.

## Cause

The CODEOWNERS file includes users or groups that are [inaccessible](https://docs.gitlab.com/ee/user/project/codeowners/reference.html#inaccessible-or-incorrect-owners) or [malformed](https://docs.gitlab.com/ee/user/project/codeowners/reference.html#malformed-owners), causing them to be ignored. 

This can affect previously working approval rules if a username or group path changes.

## Related links

[Epic: Improve CODEOWNERS validation](https://gitlab.com/groups/gitlab-org/-/epics/15598)
