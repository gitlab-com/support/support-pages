# Dotenv variables exceed default limit in GitLab pipelines

## Description

- Pipeline jobs fail when uploading artifacts as "dotenv" due to exceeding the default variable count limit.
- Users encounter the following error message:
  > WARNING: Uploading artifacts as "dotenv" to coordinator... POST https://[gitlab-instance]/api/v4/jobs/[job-id]/artifacts: 400 Bad Request (Dotenv files cannot have more than 20 variables)
- CI/CD pipelines may fail to complete successfully due to this error.

## Environment

- GitLab CI/CD pipelines using dotenv artifacts
- Projects with complex environment configurations requiring more than 20 dotenv variables

- **Impacted offerings:**
  - GitLab.com
  - GitLab Dedicated
  - GitLab Self-Managed

## Solution

On **GitLab.com** the limit cannot be changed and is set according to tier. See [GitLab.com settings](https://docs.gitlab.com/ee/user/gitlab_com/index.html#gitlab-cicd) for details.

For Self-Managed and Dedicated instances, as of GitLab 17.1, you can configure the dotenv variable limit through the GitLab UI or REST API:

### [GitLab UI](https://docs.gitlab.com/ee/administration/settings/continuous_integration.html#set-cicd-limits) 
1. Log in to your GitLab instance as an administrator.
2. Navigate to **Admin Area > Settings > CI/CD**.
3. Expand the **Continuous Integration and Deployment** section.
4. Find the **Maximum number of variables in dotenv files** setting.
5. Increase the value to your desired limit (e.g., 50).
6. Click "Save changes" at the bottom of the page.

### API

Alternatively, [use the API](https://docs.gitlab.com/ee/api/plan_limits/) to update this setting:

```bash
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/application/plan_limits?dotenv_variables=50"
```

Replace `<your_access_token>` with a valid admin access token and adjust the URL to match your GitLab instance.


### Workaround

On Self-Managed instances prior to 17.1 or if you prefer using the Rails console:

1. Access the GitLab server.
2. Start the Rails console.

   ```bash
   sudo gitlab-rails console
   ```

3. Execute the command to increase the limit.

   ```ruby
   Plan.default.actual_limits.update!(dotenv_variables: 50)
   ```

4. Exit the console by typing `exit`.

## Cause

GitLab limits the number of dotenv variables to prevent potential performance issues and abuse. The default limit is set to 20 variables, which may be insufficient for some complex pipelines or projects with extensive environment configurations.

## Additional Information

- Increasing the dotenv variable limit may impact performance for pipelines with a large number of variables.
- Consider reviewing your pipeline configuration to optimize variable usage if you frequently hit this limit.
- For self-managed instances, ensure you have adequate system resources if significantly increasing this limit.
- The limit applies per dotenv artifact, not per pipeline or project.
- If you need to use more variables, consider splitting them across multiple dotenv artifacts or using GitLab CI/CD variables for some of your configuration.

## Related Links

- [GitLab Instance Limits Documentation](https://docs.gitlab.com/ee/administration/instance_limits.html#limit-dotenv-variables)
- [Dotenv Artifacts in GitLab CI/CD](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportsdotenv)
- [GitLab CI/CD Variables](https://docs.gitlab.com/ee/ci/variables/)
- [Set CI/CD limits](https://docs.gitlab.com/ee/administration/settings/continuous_integration.html#set-cicd-limits)
- [Plan limits API](https://docs.gitlab.com/ee/api/plan_limits/)
- [Merge Request: Add UI and API to configure dotenv_variables_limit](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/150765)