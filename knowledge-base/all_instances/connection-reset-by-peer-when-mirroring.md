<!-- This template is meant to be used for specific issues with specific resolution steps. -->

# 'Connection reset by peer' error while mirroring repositories

## Description

Users encounter a "Connection reset by peer" error when attempting to mirror repositories in GitLab

<!-- 
Describe
- Break/fix: The symptoms customers will see when they encounter an issue.
- How-to: The situation when the steps in the article are needed
- Q&A: The question a customer may have 
One bullet point per item. Use a bullet point even if there's only one symptom.
-->

## Environment

<!-- Describe the environment configuration in which the description applies. -->
- `no_proxy` configuration includes port numbers in URLs

- **Impacted offerings:**
  - GitLab Dedicated
  - GitLab Self-Managed

- **Impacted versions:**
  - All current versions

## Solution OR Workaround

<!-- 
1. Change the title of this section to best fit its content: Solution or Workaround?
2. Describe steps to take to resolve or work around the issue.
-->

1. Check your `no_proxy` configuration settings for any URLs that include port numbers
3. Remove the port numbers from those URLs in the `no_proxy` configuration
4. [Restart GitLab services](https://docs.gitlab.com/ee/administration/restart_gitlab.html) if necessary

## Cause

<!-- 
Describe _what_ caused the issue to happen and _why_ it was introduced. 
Adding links to docs, blog posts, issues and other relevant materials as necessary.
-->

When the `no_proxy` configuration contains URLs with port numbers, it interferes with DNS resolution. This causes the connection to be reset when attempting to mirror repositories, resulting in the "Connection reset by peer" error.

## Additional information

- Based on testing, the `http_proxy` and `https_proxy` works fine when adding port numbers to the URLs

<!-- Describe any additional information which may assist troubleshooting. e.g. Which log file does the error show up in? -->

## Related links

<!-- Add links to feature request or bug issues here. Consider linking other resources as necessary. -->

- [GitLab Documentation on Repository Mirroring](https://docs.gitlab.com/ee/user/project/repository/mirror/)
- [Setting custom environment variables](https://docs.gitlab.com/omnibus/settings/environment-variables.html)