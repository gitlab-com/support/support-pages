# How to Assign Enterprise Agile Planning Seats

## Description

GitLab introduced Enterprise Agile Planning product on [2023-11-16](https://about.gitlab.com/blog/2023/11/16/gitlab-enterprise-agile-planning-add-on-for-all-roles/). In the beginning, the only way to provision the Enterprise Agile Planning seats is to assign users to Reporter [role](https://docs.gitlab.com/ee/user/permissions.html#roles).

The [Planner role](https://about.gitlab.com/blog/2024/11/25/introducing-gitlabs-new-planner-role-for-agile-planning-teams/) was introduced to in [GitLab 17.7](https://about.gitlab.com/releases/2024/12/19/gitlab-17-7-released/) to assign Enterprise Agile Planning seats to users.

## Environment

- **Impacted offerings:**
  - GitLab.com
  - GitLab Dedicated
  - GitLab Self-Managed

- **Impacted versions:**
  - 17.7 and above

## Solution

To assign the Enterprise Agile Planning seats to users, you can simply give the user the [Planner role](https://docs.gitlab.com/ee/user/permissions.html#roles) from the members page. 

## Related links

- [New Planner role feedback issue](https://gitlab.com/gitlab-org/gitlab/-/issues/503817)
- [New Planner user role](https://gitlab.com/gitlab-org/gitlab/-/issues/482733)
- [Approaching license user count banner does not consider EAP seats](https://gitlab.com/gitlab-org/gitlab/-/issues/504777)