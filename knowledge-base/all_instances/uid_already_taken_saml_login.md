
# `Extern UID is already taken` while using SAML to log in

## Description

- When attempting to log in with SAML, a user reports the error "Extern UID is already taken".

## Environment

<!-- Describe the environment configuration in which the description applies. -->

- This indicates that the SAML identity the user is attempting to configure is already taken by another GitLab user account.
- Often, this is another user account that belongs to the same user.

- **Impacted offerings:**
  - GitLab.com
  - GitLab Dedicated
  - GitLab Self-Managed

- **Impacted versions:**
  - All

## Solution

1. Log in to the other (incorrect) GitLab user account. You may need to [reset the password](https://gitlab.com/users/password/new)
1. Visit https://gitlab.com/-/profile/account
1. Locate the SAML provider in the "Service Sign-in" Section
1. Click "Disconnect"
1. Log out
1. Log back in to the account that should be using the SAML provider. You may need to [reset the password](https://gitlab.com/users/password/new)
1. Visit the SSO URL for your group, and log in
1. Click "Authorize"

Unlinking and linking SAML via this method can resolve most SAML-related problems.

GitLab.com group owners or Self-managed GitLab instance administrators can also update a user's `extern_uid` using the [SAML API](https://docs.gitlab.com/ee/api/saml.html#update-extern_uid-field-for-a-saml-identity).

## Cause

This indicates that the SAML identity the user is attempting to configure is already taken by another GitLab user.


## Related links

- [Unlink SAML](https://docs.gitlab.com/ee/user/group/saml_sso/#unlink-accounts)
- [Link SAML](https://docs.gitlab.com/ee/user/group/saml_sso/#link-saml-to-your-existing-gitlabcom-account)
