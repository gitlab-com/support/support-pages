# Unable to sign out of GitLab when `auto_sign_in_with_provider` is enabled

## Description

Users logging out of GitLab are automatically signed in by the Identity Provider (idP) after applying the `auto_sign_in_with_provider` setting.

## Environment

- SSO enabled

- **Impacted offerings:**
  - GitLab Self-Managed

## Workaround 

This is a known limitation [mentioned in the documentation](https://docs.gitlab.com/ee/integration/omniauth.html#:~:text=To%20prevent%20an%20infinite%20sign%2Din%20loop) for `auto_sign_in_with_provider`.

To prevent automatic sign-in, users must logout from the idP side before signing out from GitLab.

## Cause

If a user logs out of GitLab, it doesn't destroy the idP session. Since automatic sign-in with provider is enabled and the idP session still exists, the user is automatically signed back in.

There are feature requests to implement this: 
- For [SAML](https://gitlab.com/gitlab-org/gitlab/-/issues/14414)
- For [OIDC](https://gitlab.com/gitlab-org/gitlab/-/issues/504208)

