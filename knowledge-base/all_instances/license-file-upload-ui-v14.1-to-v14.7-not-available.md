# Unable to find UI to upload a license file in versions 14.1 to 14.7

## Description

When attempting to upload a license file to an activated GitLab instance that is on version 14.1 to 14.7,
the UI to upload the license file is not visible.

Additionally, the `Add License` section in the `Admin area` -> `Settings` -> `General` page
([mentioned in our docs](https://docs.gitlab.com/ee/administration/license_file.html#add-license-in-the-admin-area)), 
is not available for versions 14.1 to 14.7.

## Environment

- **Impacted offerings:**
  - GitLab Self-Managed

- **Impacted versions:**
  - 14.1 to 14.7

## Solution OR Workaround

There are two possible options:
- You can access the **Add License** page directly from the URL, `<YourGitLabURL>/admin/license/new`.
- You can [add the license via console](https://docs.gitlab.com/ee/administration/license_file.html#add-a-license-through-the-console).

## Cause

There is a link on the `Admin` -> `Subscription` widget that reads `Upload a license file`. 
Once the instance is activated, this widget is not visible, so there's no UI for uploading a license.


## Related links

- Docs MR: [Version 14.1 to 14.7 upload license file](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/100117)