# Encountering `401 Unauthorized` error while doing pushing after updating to GitLab 17.5

## Description

Performing a `git push` locally or creating commits in the GitLab UI results in a `401 Unauthorized` error.

## Environment

- GitLab with an external Gitaly node

- **Impacted offerings:**
  - GitLab Self-Managed

- **Impacted versions:**
  - 17.5.0 to current

## Solution

1. Ensure that `gitlab_shell['secret_token']` has the same value across all of your `gitlab.rb` files.
1. Ensure that `gitaly['gitlab_secret']` has the same value across all of your `gitlab.rb` files.
1. Run a reconfigure (`gitlab-ctl reconfigure`) to regenerate the `gitlab-secrets.json` file with the right values.

You may need to manually check the following values in your `gitlab-secrets.json` files to determine the right values to set:

  ```
    "gitlab_shell": {
      "secret_token": "..."
    },
  ...
    "gitaly": {
      "gitlab_secret": "..."
    }
  ```

## Cause

From GitLab 17.5, Gitaly uses its own secret token to authenticate with GitLab and no longer reuses the GitLab Shell secret token.

This change introduced a new `gitaly['gitlab_secret']` config key in the `gitlab.rb` file and a new `gitlab.gitlab_secret` config key in the `gitlab-secrets.json` file.

If `gitaly['gitlab_secret']` is not configured in `gitlab.rb`, GitLab will copy the existing GitLab Shell secret token to the new Gitaly GitLab secret token in the `gitlab-secrets.json` file.

In some cases, the secret is not copied and a new Gitaly GitLab secret token is generated instead, causing a mismatch which results in the 401 error.


## Additional information

- `gitlab-ctl reconfigure` will show a warning of `Gitaly and GitLab Shell specifies different secrets to authenticate with GitLab`.

## Related links

- https://gitlab.com/gitlab-org/omnibus-gitlab/-/issues/8688