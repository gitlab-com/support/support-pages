<!-- This template is meant to be used for specific issues with specific resolution steps. -->

# "Validation status" remains at `Validating...` while validating a site profile for on-demand DAST scan
<!-- Use sentence-case capitalization for the title. -->

## Issue

<!-- 
Describe the symptoms will see when they encounter the issue. 
One bullet point per symptom. Use a bullet point even if there's only one symptom.
-->

While [validating a site profile](https://docs.gitlab.com/ee/user/application_security/dast/on-demand_scan.html#validate-a-site-profile) for [on-demand DAST scan](https://docs.gitlab.com/ee/user/application_security/dast/on-demand_scan.html#on-demand-scans), users are seeing: 
- The "Validation status" on Site Profile remains at `Validating...`.
- The error `/tmp/validate.sh: No such file or directory` in the `validation` job log.

## Environment

<!-- Describe the circumstances in which the issue will occur. -->

- Users that redefine the environment's `HOME` directory for the runner that is performing the scan will encounter this issue. 

- **Impacted offerings:**
  - GitLab.com
  - GitLab Dedicated
  - GitLab Self-Managed

- **Impacted versions:**
  - 15.6 to 17.6

## Cause

<!-- 
Describe _what_ caused the issue to happen and _why_ it was introduced. 
Adding links to docs, blog posts, issues and other relevant materials as necessary.
-->

The `dast-runner-validation` Dockerfile expects that the `validate.sh` script exists in `/home/dast/`, which is specified in that Dockerfile as the DAST user's home directory. When the runner injects a `$HOME` variable into the job container, this overrides the logged in user's home directory, while there is no `validate.sh` script in the directory to execute. 

## Resolution OR Workaround

<!-- 
1. Change the title of this section to best fit its content: Resolution or Workaround?
2. Describe steps to take to resolve or work around the issue.
-->

- Unset the `HOME` environment variable on the runner.
- Use another runner without the `HOME` environment variable to run solely the validation job by temporarily disabling the affecting runner, and re-enabling after the job. 

## Related links

- [Feature request](https://gitlab.com/gitlab-org/gitlab/-/issues/503966) to address validation job issue with redefined runner `HOME` environment variable.
- [Feature request](https://gitlab.com/gitlab-org/gitlab/-/issues/503958) to allow DAST validation and on-demand scan jobs to be run on runners with specific tags.