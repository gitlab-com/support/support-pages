<!-- This template is meant to be used for specific issues with specific resolution steps. -->

# How to update the Primary email address of multiple users - LDAP and SAML
<!-- Use sentence-case capitalization for the title. -->

## Task

Administrators need to update Primary email addresses for multiple users. This can happen when self-managed instances change Identity providers (e.g. LDAP, SAML, etc.) and the email domain needs to be updated.

- **Applicable offerings:**
  - GitLab Self-Managed

## Instructions

Administators can use the following [Rails console](https://docs.gitlab.com/ee/administration/operations/rails_console.html) commands to bulk change user email domains. Substitute **<old_domain>** and **<new_domain>** with the old and new email domain values.

```bash
# old account primary email address
user@old_domain.com

# new account primary email address
user@new_domain.com
```

1. Start a [rails console session](https://docs.gitlab.com/ee/administration/operations/rails_console.html#starting-a-rails-console-session)
1. Perform a dry-run to confirm which emails will be updated.

   ```bash
   User.find_each do | user |
     old_email = user.email
     new_email = old_email.gsub("<old_domain>","<new_domain>") # this will only change users with the old_domain and not update other email domains
     puts "#{user.name}, #{old_email}, #{new_email}"
   end
   ```

1. If the updates look correct, apply the changes.

   ```bash
   User.find_each do | user |
     old_email = user.email
     new_email = old_email.gsub("<old_domain>","<new_domain>") # this will only change users with the old_domain and not update other email domains
     puts "#{user.name}, #{old_email}, #{new_email}"
     user.email = new_email
     user.skip_confirmation!
     user.save!
   end
   ```

## Additional information

- SAML users need to [unlink](https://docs.gitlab.com/ee/user/group/saml_sso/#unlink-accounts) and [link](https://docs.gitlab.com/ee/user/group/saml_sso/#link-saml-to-your-existing-gitlabcom-account) their new identities after making the changes.
- Users can [manually update their primary email from the UI](https://docs.gitlab.com/ee/user/profile/#change-your-primary-email)


## Related links

- [User DN and email have changed](https://docs.gitlab.com/ee/administration/auth/ldap/ldap-troubleshooting.html#user-dn-and-email-have-changed) 
