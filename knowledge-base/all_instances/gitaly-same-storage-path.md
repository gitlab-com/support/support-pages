# How to Remove Duplicate Gitaly Storage Paths

## Description

GitLab originally utilized the same Gitaly configuration across all Gitaly nodes, but GitLab no longer supports multiple Gitaly storages sharing the same path. This guide explains how to identify and resolve duplicate Gitaly storage paths on external Gitaly (not single node Omnibus) to ensure your GitLab instance operates correctly.

In most cases you will want to keep `default`

## Environment

- **Impacted offerings:**
  - GitLab Self-Managed

- **Impacted versions:**
  - GitLab 16.11 and later

## Prerequisites

- Root access to the GitLab server
- Ability to edit the `gitlab.rb` configuration file
- Access to the GitLab Rails console (optional)

## Solution

1. **Identify duplicate Gitaly storage paths in your `gitlab.rb` file:**

   ```ruby
   gitaly['configuration'] = {
     storage: [
       {
         name: 'default',
         path: '/var/opt/gitlab/git-data/repositories',
       },
       {
         name: 'duplicate-path',
         path: '/var/opt/gitlab/git-data/repositories',
       },
     ],
   }
   ```

   In this example, both storages share the same path, which is not supported.

2. **Remove or change Gitaly storage paths in your `gitlab.rb` file:**

   ```ruby
   gitaly['configuration'] = {
     storage: [
       {
         name: 'default',
         path: '/var/opt/gitlab/git-data/repositories',
       }
     ],
   }
   ```


3. **Update projects using the duplicate storage path to use the default storage:**

   Open the GitLab Rails console:

   ```bash
   sudo gitlab-rails console
   ```

   In the Rails console, execute:

   ```ruby
   Project.where(repository_storage: 'duplicate-path').update_all(repository_storage: 'default')
   ```

   This command updates all projects that are using the 'duplicate-path' storage to use the 'default' storage instead.

3. **Reconfigure GitLab to apply the changes:**

   ```bash
   sudo gitlab-ctl reconfigure
   ```

   This command applies the changes made to the configuration. Only needs to be run on nodes where Gitaly is running.

## Verification

 **Verify that no projects are using the duplicate storage path:**

   Open the GitLab Rails console again:

   ```bash
   sudo gitlab-rails console
   ```

   In the Rails console, execute:

   ```ruby
   Project.where(repository_storage: 'duplicate-path').count
   ```

   Ensure the count returned is `0`, indicating no projects are using the duplicate storage path.

5. **Monitor GitLab logs to ensure there are no related errors:**

   ```bash
   sudo gitlab-ctl tail
   ```

   This command tails the GitLab logs. Check for any issues related to Gitaly storage paths.


## Related Links

- https://gitlab.com/gitlab-org/gitaly/-/issues/5598#note_1930238681
- https://docs.gitlab.com/administration/gitaly/praefect/#gitaly
