# GitLab agent server for Kubernetes (KAS) does not start and repeatedly throws errors in GitLab 17.8.0

## Description

KAS fails to start and will repeatedly throw errors in GitLab 17.8.0. Below is a sample of the errors in `/var/log/gitlab/gitlab-kas/current`.

```
2025-01-17_06:05:29.11901 {"time":"2025-01-17T06:05:29.118906522Z","level":"INFO","msg":"Running KAS","kas":"gitlab-kas/v17.8.0/v17.8.0"}
2025-01-17_06:05:29.12061 {"time":"2025-01-17T06:05:29.120577413Z","level":"INFO","msg":"KAS shutdown done, exiting"}
2025-01-17_06:05:29.12063 {"time":"2025-01-17T06:05:29.120611647Z","level":"ERROR","msg":"Program aborted","error":"private API server: construct own private API multi URL: failed to parse OWN_PRIVATE_API_URL grpc://localhost:8155: ParseAddr(\"localhost\"): unable to parse IP"}
```

## Environment

- Installations that use the Linux (Omnibus) package (includes Docker)

- **Impacted offerings:**
  - GitLab Self-Managed

- **Impacted versions:**
  - 17.8.0

## Solution And/Or Workaround

If you are not using KAS, add the following to your `gitlab.rb`:

```
gitlab_kas['env'] = {
    'OWN_PRIVATE_API_URL' => 'grpc://127.0.0.1:8155'
}
```

If you are using KAS on a single node other than the default `127.0.0.1`, change the IP address in the above snippet to that node.

If you are using KAS on multiple nodes, follow the [documentation](https://docs.gitlab.com/ee/administration/clusters/kas.html#enable-on-multiple-nodes) to ensure you have the necessary configuration.

If you still receiving these errors with the required configuration in place, reach out to Support for additional help.

## Cause

In 17.8.0, KAS expects a configuration option to be present. This option was not added as a default to the 17.8.0 release of the Linux package, causing the error in installations that do not have the configuration option in place already.


## Related Links

[Issue link](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/issues/510) introducing the feature and reporting the error.
