# Code Owner approvals shown as optional even when fully configured

## Issue

A Code Owner approval rule is shown as optional in a merge request even when:

- [Code Owner approval on a protected branch](https://docs.gitlab.com/ee/user/project/repository/branches/protected.html#require-code-owner-approval-on-a-protected-branch) has been set up.
- A `CODEOWNERS` file is present and located in a [supported path](https://docs.gitlab.com/ee/user/project/codeowners/index.html#codeowners-file).
- Code owners have been defined and [any relevant sections are not marked optional](https://docs.gitlab.com/ee/user/project/codeowners/index.html#make-a-code-owners-section-optional).

## Environment

- **Impacted offerings:**
  - GitLab.com
  - GitLab Dedicated
  - GitLab Self-Managed

## Cause

There are no eligible code owners available to approve the merge request.

## Resolution

The most common scenarios where there are no eligible code owners are:

- There is only one code owner approver and that approver is also the author of the merge request
  or has added commits to the merge request. 
  - To resolve this, you can change the following merge request settings:
    - [Prevent approval by author](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/settings.html#prevent-approval-by-author)
    - [Prevent approvals by users who add commits](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/settings.html#prevent-approvals-by-users-who-add-commits)

- The specified code owner user or group is not a direct member of the project. Code owners cannot
  inherit from parent groups.
    - To resolve this, invite the user or group to the project.

## Related links

- Docs: [Troubleshooting Code Owners](https://docs.gitlab.com/ee/user/project/codeowners/troubleshooting.html)
- Feature request: [Acceptable CODEOWNERS Groups Should Inherit from Parent Group](https://gitlab.com/gitlab-org/gitlab/-/issues/288851/)
