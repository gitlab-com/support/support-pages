# Disabling personal access tokens for enterprise users causes 404 on GitLab pages and some integrations to break

## Description

- [Enterprise users](https://docs.gitlab.com/ee/user/enterprise_user/) experience authentication failures for various GitLab-integrated services when personal access tokens are disabled.
- Users are unable to access GitLab Pages, even as owners - a 404 error is presented.
- External services using GitLab as an identity provider fail to authenticate.
- Users cannot link their GitLab account to other GitLab services (e.g., customers.gitlab.com).

## Environment

- **Impacted offerings:**
  - GitLab.com

## Solution

1. Log in to your GitLab account as a group owner.
2. Navigate to the top-level group's "Personal access token" setting under Settings > General.
3. Uncheck the box next to "Disable personal access tokens for enterprise users".
5. Save the changes.
6. Log out and log back in to refresh your session.
7. Attempt to access the previously affected services (GitLab Pages, external integrations, etc.).

## Cause

The `Disable personal access tokens for enterprise users` setting also affects OAuth tokens, which are used by GitLab Pages and other integrations for authentication. When this setting is enabled, it breaks the authentication flow for these services.

## Additional Information

- This issue affects enterprise users whose accounts are associated with a verified domain (the account will have the `Enterprise User` label).
- Disabling personal access tokens is a security feature, but it currently has unintended consequences on other authentication methods.
- GitLab is aware of this issue and is working on a solution to allow OAuth access even when personal access tokens are disabled.

## Related Links

- [GitLab Issue: Add Option to Keep OAuth access even when "Disable Personal Access Tokens" is selected](https://gitlab.com/gitlab-org/gitlab/-/issues/412966)
- [Disable personal access tokens for Enterprise Users](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#disable-personal-access-tokens-for-enterprise-users)