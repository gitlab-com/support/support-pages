## Create a knowledge base article

> 💡 Please add any relevant links or information which inspired your knowledge base article, if applicable. 

- Zendesk ticket: 
- GitLab issue link: 
- Slack discussion link: 
- Any other relevant links: 

> 💡 If the article needs to be urgently merged, please ask a Support Manager or Staff Support Engineer for review and merge.

> 💡 Please verify some common style requirements:

- [ ] Filename in format `file_title_name.md`
- [ ] Any example logs / commands have been generalized and customer information redacted
- [ ] [Correct template](https://gitlab.com/gitlab-com/support/support-pages/-/tree/master/kb-documentation/templates) used for article
- [ ] Empty headings are removed
- [ ] Related articles follow format: [link description](link.com)

Reviewers will [publish](https://handbook.gitlab.com/handbook/support/knowledge-base/#publishing-a-kb-article) once merged.

<!-- Use for existing KBs that need to be updated -->

/label ~"Knowledge Base::Update Article" 

<!-- Use for New KBs --> 

/label ~"Knowledge Base::New Article"

<!-- Do not edit -->
/assign me
/assign_reviewer @irisb @weimeng-gtlb
/label ~"Knowledge Base" 
