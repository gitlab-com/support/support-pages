<!--
Use when:
- Documenting solutions for specific errors or problems
- Addressing system failures or malfunctions
- Providing troubleshooting steps for known issues
- Explaining how to recover from common failures

For a more detailed style guide see https://handbook.gitlab.com/handbook/support/knowledge-base/kb-style-guide/ 
-->

# Change to Zendesk article title
<!--
The title should:
- Clearly describe the error or problem
- Include specific error messages or codes if applicable
- Be searchable and SEO-friendly
- Focus on the symptom rather than the solution
Only capitalize the first word and proper nouns.
Examples:
- Repository size exceeds quota limit
- LDAP authentication fails after upgrade
-->

## Description

<!-- 
- Describe the symptoms customers will see when they encounter an issue.
- One bullet point per item. Use a bullet point even if there's only one symptom.
- List symptoms in order of significance (most impactful first)
- Include exact error messages in blockquotes
- Include specific log entries or error codes
- Note any impact on functionality or data
-->

## Environment

<!-- 
Describe the environment configuration in which the issue applies.
Examples:
- SSO enabled
- Kubernetes cluster
-->

**Impacted offerings:**
- GitLab.com
- GitLab Dedicated
- GitLab Self-Managed

**Impacted versions:**
<!--
Remove if all known / supported versions are affected 
Examples:
- 16.1 to 16.3
- 17.1 and later
- 16.4 and earlier
- 17.x
-->

## Solution

<!-- 
1. Provide clear steps to resolve the issue
2. Start each step with an action verb
3. Include commands in code blocks with comments
4. Note any required permissions or prerequisites
5. Mention any service restarts or system impacts

For multiple resolution paths:
- Option A: Steps for scenario 1
- Option B: Steps for scenario 2

If providing a temporary fix, place it under a new ### Workaround heading
-->

## Cause

<!-- 
- Explain what caused the issue and why the problem occurs
- Reference any known bugs, issues, or changes
- Note any design limitations or constraints

Use technical details when relevant but keep explanations clear
-->

## Additional Information

<!-- 
Describe any additional information which may assist troubleshooting such including: 
- Context that helps understand the issue
- Troubleshooting tips
- Any known limitations
- Performance implications
-->

## Related Links

<!-- 
Add links to relevant resources such as GitLab issues or articles. Example format:
[Related topics](https://docs.gitlab.com/ee/development/documentation/topic_types/index.html#related-topics)
-->
