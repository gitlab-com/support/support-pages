<!-- 
Use when:
- Providing step-by-step instructions for completing a specific task
- Teaching users how to implement a feature or configuration
- Explaining processes that have a clear beginning and end
- Walking through setup or installation procedures

For a more detailed style guide see https://handbook.gitlab.com/handbook/support/knowledge-base/kb-style-guide/ 
-->

# Change to Zendesk article title
<!-- 
Only capitalize the first word and proper nouns.
The title should:
- Clearly indicate the specific task being accomplished
- Include any key technologies/features involved
- Start with "How to"

Example: How to configure LDAP authentication
-->

## Description

<!-- 
- Clearly state when and why a user would need these instructions
- If applicable, mention the end result or benefits
- Focus on the user's goal rather than the feature
-->

## Environment

<!-- 
Describe the environment configuration in which the issue applies.
Examples:
- SSO enabled
- Kubernetes cluster
-->

**Impacted offerings:**
- GitLab.com
- GitLab Dedicated
- GitLab Self-Managed

**Impacted versions:**
<!--
Remove if all known / supported versions are affected 
Examples:
- 16.1 to 16.3
- 17.1 and later
- 16.4 and earlier
- 17.x
-->

## Prerequisites
<!--
Optional

List any required:
- Access levels or permissions
- System requirements
- Prior configuration
- Related features that must be enabled
- Knowledge or skills needed
- Tools or software that must be installed
-->

## Solution
<!--
1. Break down complex procedures into clear, sequential steps
2. Start each step with an action verb
3. Include example commands in code blocks
4. Add notes or warnings using blockquotes for important cautions
5. For branching scenarios, use sub-steps:
   - Option A: Steps for scenario 1
   - Option B: Steps for scenario 2
6. Include expected outcome after key steps
7. Reference existing documentation when available
-->

## Verification

<!--
- List steps to verify the solution was successful
- Include expected outputs or behaviors
- Mention common indicators of success
- Note any post-implementation tasks
- Include troubleshooting tips for common issues
-->

## Additional Information

<!--
- Add context that helps understand the solution
- Include best practices and recommendations
- Note any performance implications
- List known limitations or edge cases
- Provide alternatives for different scenarios
-->

## Related Links

<!--
Add links to relevant resources such as GitLab issues or articles. Format:
[Related topics style guide](https://docs.gitlab.com/ee/development/documentation/topic_types/index.html#related-topics)
-->