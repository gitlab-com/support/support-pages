<!-- 
Use when:
- Explaining concepts or behaviors
- Clarifying how features work
- Answering common customer inquiries
- providing information that doesn't require step-by-step instructions

For a more detailed style guide see https://handbook.gitlab.com/handbook/support/knowledge-base/kb-style-guide/ 
-->

# Change to Zendesk article title

<!-- 
Only capitalize the first word and proper nouns. End with a question mark (?)
Examples:
- How does GitLab handle rate limiting?

The title should:
- Be phrased as a clear, specific question
- Use common terms users would search for
- Avoid compound or multiple questions
-->

## Overview

<!--
Context for why the question is asked. 
For example, being triggered by an announcement sent to users or recent release.
-->

## Environment

<!-- 
Describe any specific environment details relevant to this Q&A.
Include any:
- Installation types
- Configuration requirements
- Feature dependencies
- System requirements
-->

**Impacted offerings:**
- GitLab.com
- GitLab Dedicated
- GitLab Self-Managed

**Impacted versions:**

<!--
Remove if all known / supported versions are affected 
Examples:
- 16.1 to 16.3
- 17.1 and later
- 16.4 and earlier
- 17.x
-->

## Answer

<!-- 
- Provide a clear, comprehensive answer
- Reference official documentation as necessary
- Use examples when helpful
-->

## Additional information

<!-- 
Any extra context related to the Q&A 
-->

## Related Links

<!-- 
Add links to relevant resources such as GitLab issues or articles. Example format:
[Related topics style guide](https://docs.gitlab.com/ee/development/documentation/topic_types/index.html#related-topics)
-->
